package improve

import (
	"testing"
	"time"
)

func TestForLoop(t *testing.T) {
	// 老版本 闭包
	for i := 0; i < 10; i++ {
		go func() {
			t.Log(i)
		}()
	}

	time.Sleep(time.Second)

	// 历史解决
	for i := 0; i < 10; i++ {
		go func(i int) {
			t.Log(i)
		}(i)
	}

	time.Sleep(time.Second)

	// 老版本引入临时变量指针
	var pIntvals []*int
	Intvals := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	for _, intval := range Intvals {
		pIntvals = append(pIntvals, &intval)
	}

	for _, v := range pIntvals {
		t.Log(*v)
	}

	// 1.21 运行时开启 GOEXPERIMENT=loopvar
	for i := 0; i < 10; i++ {
		go func() {
			t.Log(i)
		}()
	}

	time.Sleep(time.Second)
}
