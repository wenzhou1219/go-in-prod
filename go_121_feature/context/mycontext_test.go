package mycontext

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"testing"
	"time"
)

// 测试context cause
func TestContextCause(t *testing.T) {
	err := fmt.Errorf("cause error")
	var wg sync.WaitGroup

	ctx, cancel := context.WithCancelCause(context.Background())
	wg.Add(1)
	go func() {
		defer wg.Done()
		time.Sleep(10 * time.Second)
		cancel(err)
	}()

	wg.Wait()
	fmt.Println(context.Cause(ctx))
	fmt.Println(ctx.Err())
}

// 测试context afterfunc (超时 or 取消时调用)
// 支持注册context done时对应的回调函数，单独协程运行，不阻塞返回
func TestContextAfterFunc1(t *testing.T) {
	var wg sync.WaitGroup

	ctx, cancel := context.WithCancel(context.Background())
	context.AfterFunc(ctx, func() {
		t.Log("Stop down call1")
	})
	context.AfterFunc(ctx, func() {
		t.Log("Stop down call2")
	})

	wg.Add(1)
	go func() {
		defer wg.Done()
		time.Sleep(2 * time.Second)
		cancel()
		time.Sleep(1 * time.Second)
	}()

	wg.Wait()
}

// 基于ctx1创建新的ctx，ctx2 取消也会触发ctx
func WithFirstCancel(ctx1, ctx2 context.Context) (context.Context, context.CancelCauseFunc) {
	ctx, cancel := context.WithCancelCause(ctx1)

	// ctx2 取消也会触发ctx1
	stopf := context.AfterFunc(ctx2, func() {
		cancel(context.Cause(ctx2))
	})

	return ctx, func(cause error) {
		cancel(cause)
		stopf() // 解除绑定回调
	}
}

func TestContextAfterFuncMerge(t *testing.T) {
	ctx1, _ := context.WithCancel(context.Background())
	ctx2, _ := context.WithTimeout(context.Background(), 2*time.Second)

	ctx, cancel := WithFirstCancel(ctx1, ctx2)
	go func() {
		time.Sleep(5 * time.Second)
		cancel(errors.New("ctx1 cancel"))
	}()

	select {
	case <-ctx.Done():
		t.Log(context.Cause(ctx))
	}
}
