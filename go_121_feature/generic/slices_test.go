package generic

import (
	"slices"
	"testing"
)

func TestSlices(t *testing.T) {
	x := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}

	t.Log("slice 二分查找")
	index, found := slices.BinarySearch(x, 5)
	t.Log(index, found)

	t.Log("slice 二分查找第一个大于6的元素")
	y := []int{1, 2, 3, 6, 6, 6, 7, 8, 9}
	index, found = slices.BinarySearchFunc(y, 6, func(a, b int) int {
		if a < b {
			return -1
		} else {
			return 0
		}
	})
	t.Log(index, found)

	t.Log("slice 排序")
	z1 := []int{3, 7, 8, 2, 1, 6, 5, 4, 9}
	slices.Sort(z1)
	t.Log(z1)

	t.Log("slice 自定义排序函数")
	z2 := []int{3, 7, 8, 2, 1, 6, 5, 4, 9}
	slices.SortFunc(z2, func(a, b int) int {
		if a < b {
			return -1
		} else if a > b {
			return 1
		} else {
			return 0
		}
	})
	t.Log(z2)

	t.Log("slice 从切片中删除未使用的容量- data len cap")
	c1 := make([]int, 0, 10)
	c1 = append(c1, 1)
	c1 = append(c1, 2)

	t.Log(c1, len(c1), cap(c1))
	ret := slices.Clip(c1)
	t.Log(ret, len(ret), cap(ret))

	t.Log("slice 浅拷贝- data elem_address")
	c2 := []int{1, 2, 3}

	t.Log(c2, &c2[0], &c2[1], &c2[2])
	ret = slices.Clone(c2)
	t.Log(ret, &ret[0], &ret[1], &ret[2])

	t.Log("slice 连续相等的元素替换为单个元素- data")
	c3 := []int{1, 2, 2, 2, 3, 3, 3}

	t.Log(c3)
	ret = slices.Compact(c3)
	t.Log(ret)
}
