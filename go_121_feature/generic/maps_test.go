package generic

import (
	"maps"
	"testing"
)

func TestMaps(t *testing.T) {
	t.Log("map 浅拷贝- data elem_address")
	c1 := make(map[int]int, 10)
	c1[0] = 1
	c1[1] = 2

	c2 := maps.Clone(c1)

	t.Log(c2)
	c2[0] = 3
	t.Log(c1)

	t.Log("map 复制- 覆盖目标中key相同记录")
	c3 := map[int]int{0: 1, 1: 2, 2: 3}
	c4 := map[int]int{2: 6, 3: 6}

	t.Log(c4)
	maps.Copy(c4, c3)
	t.Log(c4)

	t.Log("map 判断是否相等")
	m1 := map[int]int{0: 1, 1: 2, 2: 3}
	m2 := map[int]int{0: 2, 1: 4, 2: 6}

	v1 := maps.Equal(m1, m2)
	v2 := maps.EqualFunc(m1, m2, func(a, b int) bool {
		if a == b/2 {
			return true
		}
		return false
	})

	t.Log(v1, v2)
}
