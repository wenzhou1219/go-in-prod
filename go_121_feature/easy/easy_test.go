package easy

import (
	"cmp"
	"testing"
)

func TestMapClear(t *testing.T) {
	m := make(map[string]string)
	m["a"] = "a"
	m["b"] = "b"
	m["c"] = "c"

	t.Log(m)

	// 遍历删除map中的元素
	for k, _ := range m {
		delete(m, k)
	}

	t.Log(m)

	m["a"] = "a"
	m["b"] = "b"
	m["c"] = "c"

	// 重新赋空值
	m = make(map[string]string)
	t.Log(m)

	// 1.21 在直接清空
	clear(m)
	t.Log(m)
}

func TestMinMax(t *testing.T) {
	// 老版本自己定义min max实现
	myMin := func(a, b int) int {
		if a < b {
			return a
		}

		return b
	}

	t.Log(myMin(1, 2))

	// 1.21 直接调用
	t.Log(min(1, 2))
	t.Log(max(1, 2))

	// 1.21 引入cmp
	t.Log(cmp.Compare("12", "34"))
}
