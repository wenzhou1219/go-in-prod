package log

import (
	"context"
	"log/slog"
	"os"
	"testing"
)

// https://github.com/golang/go/issues/58243

func TestSlog(t *testing.T) {
	t.Log("文本日志，默认info级别")
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, nil)))

	slog.Info("log message1", "log_key1", "log_val1", "log_key2", "log_val2")
	slog.Error("log message2", slog.String("log_key1", "log_val1"), slog.Bool("log_key2", true))
	slog.Debug("log message3", "log_key1", "log_val1", "log_key2", "log_val2")

	t.Log("json日志")
	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stderr, nil)))

	slog.Info("log message1", "log_key1", "log_val1", "log_key2", "log_val2")
	slog.Error("log message2", slog.String("log_key1", "log_val1"), slog.Bool("log_key2", true))

	t.Log("全局属性注入")
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, nil)))

	logger := slog.With("global_key", "global_val")
	logger.Warn("log message1", "log_key1", "log_val1", "log_key2", "log_val2")
	logger.Warn("log message2", "log_key1", "log_val1", "log_key2", "log_val2")

	t.Log("opts 设置日志输出级别")
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{Level: slog.LevelDebug})))
	slog.Debug("log message3", "log_key1", "log_val1", "log_key2", "log_val2")

	t.Log("context传递上下文信息,自定义handler处理trace等信息")
	slog.InfoContext(context.Background(), "log message1", "log_key1", "log_val1", "log_key2", "log_val2")
}
