package iter

import (
	"fmt"
	"strings"
	"testing"
	"unicode"
)

// https://mp.weixin.qq.com/s/ApeykXCHZ6EV5tBQ_uhE6A

func TestStringsSeq(t *testing.T) {
	test_data := "hello\nworld\ntest"
	test_data1 := "hello-world-test"
	test_data2 := "hello world\ntest"

	// 1. 切分遍历字符串 默认换行分割, 返回一次性使用迭代器，每行包括行结束行 \n
	for line := range strings.Lines(test_data) {
		t.Log(line)
	}

	// 2. 切分遍历字符串 自定义符号分割，一次性使用迭代器，不包括分隔符
	for part := range strings.SplitSeq(test_data1, "-") {
		t.Log(part)
	}

	// 切分遍历字符串 自定义符号分割，一次性使用迭代器，包括分隔符
	for part := range strings.SplitAfterSeq(test_data1, "-") {
		t.Log(part)
	}

	// 3. 空白字符分割(包括空格 换行等 unicode.IsSpace定义)
	for part := range strings.FieldsSeq(test_data2) {
		t.Log(part)
	}

	// 自定义空白字符分割
	f := func(c rune) bool {
		return !unicode.IsLetter(c) && !unicode.IsNumber(c)
	}

	for part := range strings.FieldsFuncSeq(s, f) {
		fmt.Println(part)
	}

}
