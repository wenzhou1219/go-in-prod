package fan_inout

import (
	"sync"
	"testing"
	"time"
)

/*
 * 共享channel 消费，默认系统分发策略
 */
type FUNC func(data int) int

func fanOut(dataChan chan int, fs ...FUNC) []chan int {
	cs := make([]chan int, 0)

	for i := 0; i < len(fs); i++ {
		// 每个函数创建一个输出通道
		out := make(chan int)
		cs = append(cs, out)

		// 多个函数共享一个输入通道
		go func(index int) {
			defer close(out)

			for data := range dataChan {
				out <- fs[index](data)
			}
		}(i)
	}

	return cs
}

func TestFanOut(t *testing.T) {
	// 产生测试数据
	dataChan := make(chan int)
	go func() {
		for i := 0; i < 5; i++ {
			dataChan <- i
			time.Sleep(time.Second)
		}

		close(dataChan)
	}()

	// Fan out
	out := fanOut(dataChan,
		func(data int) int {
			return data * data
		},
		func(data int) int {
			return data * 2
		})

	// Fan in 打印结果
	var wg = sync.WaitGroup{}
	wg.Add(len(out))

	for i := 0; i < len(out); i++ {
		go func(index int) {
			defer wg.Done()
			for d := range out[index] {
				t.Logf("Channel %d out->%d\n", index, d)
			}
		}(i)
	}

	wg.Wait()
}

/*
 * 共享channel 消费，自定义分发策略
 */

// Split 1->N，分发一个channel到多个channel， 轮询分发
func Split(ch <-chan int, n int) []chan int {
	cs := make([]chan int, 0)
	for i := 0; i < n; i++ {
		cs = append(cs, make(chan int))
	}

	distributeToChannels := func(ch <-chan int, cs []chan int) {
		// 转发结束时关闭全部输出通道
		defer func(cs []chan int) {
			for _, c := range cs {
				close(c)
			}
		}(cs)

		// 轮询分发
		i := -1
		for val := range ch {
			i = (i + 1) % len(cs)
			cs[i] <- val
		}
	}

	go distributeToChannels(ch, cs)

	return cs
}

// Split 1->N，分发一个channel到多个channel， 指定策略分发
type SPLIT_FUNC func(data int) int

func SplitF(ch <-chan int, n int, f SPLIT_FUNC) []chan int {
	cs := make([]chan int, 0)
	for i := 0; i < n; i++ {
		cs = append(cs, make(chan int))
	}

	distributeToChannels := func(ch <-chan int, cs []chan int) {
		// 转发结束时关闭全部输出通道
		defer func(cs []chan int) {
			for _, c := range cs {
				close(c)
			}
		}(cs)

		// 轮询分发
		i := 0
		for val := range ch {
			i = f(val)
			if i < 0 || i >= len(cs) {
				panic("Split function error!")
			}

			cs[i] <- val
		}
	}

	go distributeToChannels(ch, cs)

	return cs
}

// Clone 1->N，复制一个channel到多个channel
func Clone(ch <-chan int, n int) []chan int {
	cs := make([]chan int, 0)
	for i := 0; i < n; i++ {
		cs = append(cs, make(chan int))
	}

	distributeToChannels := func(ch <-chan int, cs []chan int) {
		// 转发结束时关闭全部输出通道
		defer func(cs []chan int) {
			for _, c := range cs {
				close(c)
			}
		}(cs)

		// 输出的每个通道都【复制】一份数据
		for val := range ch {
			for _, c := range cs {
				c <- val
			}
		}
	}

	go distributeToChannels(ch, cs)

	return cs
}

/*
 * channel 合并
 */

// N->1，合并多个channel到一个channel， Fan-in实现
func Merge(cs []chan int) <-chan int {
	var wg sync.WaitGroup
	out := make(chan int)

	// 拷贝每个通道输出到统一输出out channel
	output := func(c <-chan int) {
		defer wg.Done()
		for n := range c {
			out <- n
		}
	}

	wg.Add(len(cs))
	for _, c := range cs {
		go output(c)
	}

	// 起监控, 确保所有输出合并完成后关闭输出out channel
	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}

func fanOutSplit(dataChan chan int, fs ...FUNC) []chan int {
	splitChan := SplitF(dataChan, len(fs), func(data int) int {
		return 0
	})

	cs := make([]chan int, 0)

	for i := 0; i < len(fs); i++ {
		// 每个函数创建一个输出通道
		out := make(chan int)
		cs = append(cs, out)

		// 多个函数共享一个输入通道
		go func(index int) {
			defer close(out)

			for data := range splitChan[index] {
				out <- fs[index](data)
			}
		}(i)
	}

	return cs
}

func fanOutClone(dataChan chan int, fs ...FUNC) []chan int {
	splitChan := Clone(dataChan, len(fs))

	cs := make([]chan int, 0)

	for i := 0; i < len(fs); i++ {
		// 每个函数创建一个输出通道
		out := make(chan int)
		cs = append(cs, out)

		// 多个函数共享一个输入通道
		go func(index int) {
			defer close(out)

			for data := range splitChan[index] {
				out <- fs[index](data)
			}
		}(i)
	}

	return cs
}

func TestFanOutSplitIn(t *testing.T) {
	// 产生测试数据
	dataChan := make(chan int)
	go func() {
		for i := 0; i < 5; i++ {
			dataChan <- i
			time.Sleep(time.Second)
		}

		close(dataChan)
	}()

	// Fan out
	out := fanOutSplit(dataChan,
		func(data int) int {
			return data * data
		},
		func(data int) int {
			return data * 2
		})

	// Fan in 打印结果
	for d := range Merge(out) {
		t.Logf("Channel out->%d\n", d)
	}
}

func TestFanOutCloneIn(t *testing.T) {
	// 产生测试数据
	dataChan := make(chan int)
	go func() {
		for i := 0; i < 5; i++ {
			dataChan <- i
			time.Sleep(time.Second)
		}

		close(dataChan)
	}()

	// Fan out
	out := fanOutClone(dataChan,
		func(data int) int {
			return data * data
		},
		func(data int) int {
			return data * 2
		})

	// Fan in 打印结果
	for d := range Merge(out) {
		t.Logf("Channel out->%d\n", d)
	}
}
