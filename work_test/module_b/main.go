package module_b

import (
  "fmt"
)

func ModuleBFunc() {
    fmt.Println("Hello from module B")
}

func main() {
     ModuleBFunc()
}
