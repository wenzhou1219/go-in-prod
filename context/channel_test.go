package context

import (
	"sync"
	"testing"
	"time"
)

// 多个任务并行控制，等待所有任务完成
func TestTaskControl(t *testing.T) {

	taskNum := 3

	wg := sync.WaitGroup{}
	wg.Add(taskNum)

	for i := 0; i < taskNum; i++ {
		go func(taskNo int) {
			t.Logf("Task %d run\n", taskNo)

			wg.Done()
		}(i)
	}

	wg.Wait()
}

// 执行任务超时后退出
func TestTimeoutControl(t *testing.T) {
	data := make(chan int, 10)

	go func(data chan int) {
		for {
			select {
			case val, ok := <-data:
				if !ok {
					t.Logf("Channel closed——revice exit signal ！！！")
					return
				}

				t.Logf("Revice data %d\n", val)

			case <-time.After(2 * time.Second):
				t.Log("Task time out, exit!\n")
				return
			}
		}
	}(data)

	go func() {
		data <- 1
		time.Sleep(3 * time.Second)
		data <- 2
	}()

	time.Sleep(10 * time.Second)
}

func TestCancelControl(t *testing.T) {
	data := make(chan int, 10)

	go func(data chan int) {
		for {
			select {
			case val, ok := <-data:
				if !ok {
					t.Logf("Channel closed ！！！")
					return
				}

				t.Logf("Revice data %d\n", val)
			}
		}
	}(data)

	go func() {
		data <- 1
		time.Sleep(1 * time.Second)
		data <- 2

		close(data)
	}()

	time.Sleep(10 * time.Second)
}

func TestMixControl(t *testing.T) {
	data := make(chan int, 10)
	defer close(data)
	exit := make(chan struct{})

	taskNum := 3

	wg := sync.WaitGroup{}
	wg.Add(taskNum)

	for i := 0; i < taskNum; i++ {
		go func(taskNo int, data chan int, exit chan struct{}) {
			defer wg.Done()

			for {
				select {
				case val, ok := <-data:
					if !ok {
						t.Logf("Task %d channel closed ！！！", taskNo)
						return
					}

					t.Logf("Task %d  revice data %d\n", taskNo, val)

				case <-exit:
					t.Logf("Task %d  revice exit signal!\n", taskNo)
					return
				}
			}

		}(i, data, exit)
	}

	go func() {
		data <- 1
		data <- 2
		data <- 3
		time.Sleep(1 * time.Second)
		data <- 4
		data <- 5
		data <- 6

		close(exit)
	}()

	wg.Wait()
}
