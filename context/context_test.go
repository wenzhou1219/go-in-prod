package context

import (
	"context"
	"sync"
	"testing"
	"time"
)

func TestContextCancelControl(t *testing.T) {
	data := make(chan int, 10)
	defer close(data)

	ctx, cancel := context.WithCancel(context.Background())

	taskNum := 3

	wg := sync.WaitGroup{}
	wg.Add(taskNum)

	for i := 0; i < taskNum; i++ {
		go func(taskNo int, data chan int, ctx context.Context) {
			defer wg.Done()

			for {
				select {
				case val, ok := <-data:
					if !ok {
						t.Logf("Task %d channel closed ！！！", taskNo)
						return
					}

					t.Logf("Task %d  revice data %d\n", taskNo, val)

				case <-ctx.Done():
					t.Logf("Task %d  revice exit signal!\n", taskNo)
					return
				}
			}

		}(i, data, ctx)
	}

	go func() {
		data <- 1
		data <- 2
		data <- 3
		time.Sleep(1 * time.Second)
		data <- 4
		data <- 5
		data <- 6

		cancel()
	}()

	wg.Wait()
}

func TestContextTimeoutControl(t *testing.T) {
	data := make(chan int, 10)
	defer close(data)

	ctx, _ := context.WithTimeout(context.Background(), 3*time.Second)

	taskNum := 3

	wg := sync.WaitGroup{}
	wg.Add(taskNum)

	for i := 0; i < taskNum; i++ {
		go func(taskNo int, data chan int, ctx context.Context) {
			defer wg.Done()

			for {
				select {
				case val, ok := <-data:
					if !ok {
						t.Logf("Task %d channel closed ！！！", taskNo)
						return
					}

					t.Logf("Task %d  revice data %d\n", taskNo, val)

				case <-ctx.Done():
					t.Logf("Task %d  revice exit signal!\n", taskNo)
					return
				}
			}

		}(i, data, ctx)
	}

	go func() {
		data <- 1
		data <- 2
		data <- 3
		time.Sleep(1 * time.Second)
		data <- 4
		data <- 5
		data <- 6
	}()

	wg.Wait()
}

func TestContextValueControl(t *testing.T) {

	ctx, cancel := context.WithCancel(context.WithValue(context.Background(), "testkey", "testvalue"))

	taskNum := 1
	wg := sync.WaitGroup{}
	wg.Add(taskNum)

	go func(ctx context.Context) {
		defer wg.Done()

		for {
			select {
			case <-ctx.Done():
				t.Logf("Task revice exit signal, ctx value:%s!\n", ctx.Value("testkey"))
				return
			}
		}

	}(ctx)

	go func() {
		time.Sleep(3 * time.Second)
		cancel()
	}()

	wg.Wait()
}

func TestContextMixCancelControl(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	type FUNC func(ctx context.Context)
	runFunc := func(ctx context.Context, fname string, f FUNC) {

		t.Logf("Task %s start!\n", fname)
		f(ctx)

		for {
			select {

			case <-ctx.Done():
				t.Logf("Task %s  revice exit signal!\n", fname)
				return
			}
		}

	}

	go runFunc(ctx, "A", func(ctx context.Context) {
		go runFunc(ctx, "B", func(ctx context.Context) {
			go runFunc(ctx, "C", func(ctx context.Context) {
				go runFunc(ctx, "D", func(ctx context.Context) {})
			})
		})

		go runFunc(ctx, "E", func(ctx context.Context) {
			go runFunc(ctx, "F", func(ctx context.Context) {
				go runFunc(ctx, "G", func(ctx context.Context) {})
			})
		})
	})

	go func() {
		time.Sleep(3 * time.Second)
		cancel()
	}()

	time.Sleep(10 * time.Second)
}

func TestContextMixCancelControl2(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	type FUNC func(ctx context.Context)
	runFunc := func(ctx context.Context, fname string, f FUNC) {

		t.Logf("Task %s start!\n", fname)
		f(ctx)

		for {
			select {

			case <-ctx.Done():
				t.Logf("Task %s  revice exit signal!\n", fname)
				return
			}
		}

	}

	ctxb, cancelb := context.WithCancel(ctx)
	ctxe, cancele := context.WithCancel(ctx)

	go runFunc(ctx, "A", func(ctx context.Context) {

		go runFunc(ctxb, "B", func(ctx context.Context) {
			go runFunc(ctx, "C", func(ctx context.Context) {
				go runFunc(ctx, "D", func(ctx context.Context) {})
			})
		})

		go runFunc(ctxe, "E", func(ctx context.Context) {
			go runFunc(ctx, "F", func(ctx context.Context) {
				go runFunc(ctx, "G", func(ctx context.Context) {})
			})
		})
	})

	go func() {
		time.Sleep(3 * time.Second)
		cancele()
		time.Sleep(3 * time.Second)
		cancelb()
	}()

	time.Sleep(10 * time.Second)
}
