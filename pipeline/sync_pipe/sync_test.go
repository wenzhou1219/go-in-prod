package sync_pipe

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"sort"
	"strings"
	"sync"
	"testing"
	"time"
)

// 莎士比亚的十四行诗
var LINES = `Shakespeare Sonnet 12
When I do count the clock that tells the time
And see the brave day sunk in hideous night
When I behold the violet past prime
And sable curls all silver'd o'er with white
When lofty trees I see barren of leaves
Which erst from heat did canopy the herd
And summer's green, all girded up in sheaves
Born on the bier with white and bristly beard
Then of thy beauty do I question make
That thou among the wastes of time must go
Since sweets and beauties do themselves forsake
And die as fast as they see others grow
And nothing 'gainst Time's scythe can make defence
Save breed, to brave him when he takes thee hence`

// 1.按行分割后按照空格分割
// 2.统计每个单词出现的次数
// 3.按照次数排序
// 4.输出出现次数最多的前三单词
func TestSimpleWordCount(t *testing.T) {
	r := bufio.NewReader(strings.NewReader(LINES))

	wordStat := make(map[string]int)

	for {
		// 按行分割
		l, _, err := r.ReadLine()
		if err != nil {
			if err != io.EOF {
				t.Log(err.Error())
			}

			break
		}

		// 拆分单词并统计词频
		words := strings.Split(string(l), " ")
		for _, word := range words {
			if v, ok := wordStat[word]; ok {
				wordStat[word] = v + 1
			} else {
				wordStat[word] = 1
			}
		}
	}

	// 倒排
	var wordStatSlice []struct {
		cnt  int
		word string
	}

	for k, v := range wordStat {
		wordStatSlice = append(wordStatSlice, struct {
			cnt  int
			word string
		}{cnt: v, word: k})
	}

	sort.Slice(wordStatSlice, func(i, j int) bool {
		return wordStatSlice[i].cnt > wordStatSlice[j].cnt
	})

	// 输出TOP 3
	for i := 0; i < 3; i++ {
		t.Logf("单词:%s - 出现次数:%d", wordStatSlice[i].word, wordStatSlice[i].cnt)
	}
}

// --- 函数版本封装

// 产生数据

func dataSource(ctx context.Context) (chan any, error) {
	out := make(chan any)

	go func() {
		defer close(out)

		r := bufio.NewReader(strings.NewReader(LINES))
		input, err := io.ReadAll(r)
		if err != nil {
			log.Printf("Read error %v\n", err.Error())
			return
		}

		out <- string(input)
	}()

	return out, nil
}

func dataTimerSource(ctx context.Context) (chan any, error) {
	out := make(chan any)

	go func() {
		defer close(out)

		for {
			select {
			case <-time.After(time.Second):
				r := bufio.NewReader(strings.NewReader(LINES))
				input, err := io.ReadAll(r)
				if err != nil {
					log.Printf("Read error %v\n", err.Error())
					return
				}

				out <- string(input)

			case <-ctx.Done():
				log.Println("Receive exit msg!")
				return
			}
		}
	}()

	return out, nil
}

// 按行分割
func splitByLine(ctx context.Context, params any) (any, error) {
	if v, ok := params.(string); !ok {
		return nil, errors.New("Split Input type error ")
	} else {
		return strings.Split(v, "\n"), nil
	}
}

// 拆分单词并统计词频
func countByWord(ctx context.Context, params any) (any, error) {
	if v, ok := params.([]string); !ok {
		return nil, errors.New("Count Input type error ")
	} else {
		wordStat := make(map[string]int)

		for _, l := range v {
			words := strings.Split(string(l), " ")
			for _, word := range words {
				if v, ok := wordStat[word]; ok {
					wordStat[word] = v + 1
				} else {
					wordStat[word] = 1
				}
			}
		}

		return wordStat, nil
	}
}

// 排序计算 倒序
func sortByCount(ctx context.Context, params any) (any, error) {
	if wordStat, ok := params.(map[string]int); !ok {
		return nil, errors.New("Sort Input type error ")
	} else {
		var wordStatSlice []struct {
			cnt  int
			word string
		}

		for k, v := range wordStat {
			wordStatSlice = append(wordStatSlice, struct {
				cnt  int
				word string
			}{cnt: v, word: k})
		}

		// 倒排
		sort.Slice(wordStatSlice, func(i, j int) bool {
			return wordStatSlice[i].cnt > wordStatSlice[j].cnt
		})

		// 取TOP 3
		return wordStatSlice[:3], nil
	}
}

// 输出
func outTop3(ctx context.Context, params any) (any, error) {
	if v, ok := params.([]struct {
		cnt  int
		word string
	}); !ok {
		return nil, errors.New("Output Input type error ")
	} else {
		for i, _ := range v {
			fmt.Printf("单词:%s - 出现次数:%d\n", v[i].word, v[i].cnt)
		}

		return nil, nil
	}
}

type PipeSourceFunc func(ctx context.Context) (chan any, error)
type PipeProcessFunc func(ctx context.Context, params any) (any, error)

func PipeProcessBuildAndRun(ctx context.Context, input PipeSourceFunc, funcs ...PipeProcessFunc) {
	var err error = nil

	// 输入
	dataChan, err := input(ctx)
	if err != nil {
		log.Printf("source error-%v\n", err.Error())
		return
	}

	// pipeline构建和执行
	for data := range dataChan {
		// 依次执行函数，上一个函数返回结果当做下个函数参数
		for _, processFunc := range funcs {
			data, err = processFunc(ctx, data)

			// 错误集中处理，这里选择提前退出
			if err != nil {
				log.Printf("process error-%v\n", err.Error())
				return
			}
		}
	}
}

func PipeProcessBuildAndRunN(ctx context.Context, input PipeSourceFunc, maxCnt int, funcs ...PipeProcessFunc) {

	var err error = nil

	// 输入
	dataChan, err := input(ctx)
	if err != nil {
		log.Printf("source error-%v\n", err.Error())
		return
	}

	var wg = sync.WaitGroup{}
	wg.Add(maxCnt)

	// pipeline构建和执行
	// maxCnt个协程同时消费处理
	for i := 0; i < maxCnt; i++ {
		go func() {
			defer wg.Done()

			var err error = nil

			for data := range dataChan {
				// 依次执行函数，上一个函数返回结果当做下个函数参数
				for _, processFunc := range funcs {
					data, err = processFunc(ctx, data)

					// 错误集中处理，这里选择提前退出
					if err != nil {
						log.Printf("process error-%v\n", err.Error())
						return
					}
				}
			}

		}()
	}

	wg.Wait()
}

func TestSimpleWordCount2(t *testing.T) {
	ctx := context.Background()
	PipeProcessBuildAndRun(ctx, dataSource, splitByLine, countByWord, sortByCount, outTop3)
}

func TestSimpleWordCount3(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	// 定时通知退出
	go func() {
		time.Sleep(5 * time.Second)
		cancel()
	}()

	PipeProcessBuildAndRunN(ctx, dataTimerSource, 3, splitByLine, countByWord, sortByCount, outTop3)
}

// --- OO版本封装

// 输入

type TimerSource struct {
	data string
}

func NewTimerSource(data string) ISource {
	return &TimerSource{data}
}

func (s *TimerSource) Process(ctx context.Context) (<-chan any, error) {
	out := make(chan any, 10)

	go func() {
		defer close(out)

		for {
			select {
			// 定时输出
			case <-time.After(1 * time.Second):
				r := bufio.NewReader(strings.NewReader(s.data))
				input, err := io.ReadAll(r)
				if err != nil {
					log.Printf("Read error %v", err.Error())
					return
				}

				out <- string(input)

			// 外部中断
			case <-ctx.Done():
				log.Println("Notify to exit source!!!")
				return
			}
		}
	}()

	return out, nil
}

// 输出
type ConsoleSink struct {
}

func NewConsoleSink() ISink {
	return &ConsoleSink{}
}

func (s *ConsoleSink) Process(ctx context.Context, params any) error {
	if v, ok := params.([]struct {
		cnt  int
		word string
	}); !ok {
		return errors.New("Sink Input type error ")
	} else {
		for i, _ := range v {
			fmt.Printf("单词:%s - 出现次数:%d\n", v[i].word, v[i].cnt)
		}

		return nil
	}
}

// 按行分割

type SplitProcessor struct{}

func (p *SplitProcessor) Process(ctx context.Context, params any) (any, error) {
	if v, ok := params.(string); !ok {
		return nil, errors.New("Split Input type error ")
	} else {
		return strings.Split(v, "\n"), nil
	}
}

// 拆分单词并统计词频
type CountProcessor struct{}

func (p *CountProcessor) Process(ctx context.Context, params any) (any, error) {
	if v, ok := params.([]string); !ok {
		return nil, errors.New("Count Input type error ")
	} else {
		wordStat := make(map[string]int)

		for _, l := range v {
			words := strings.Split(string(l), " ")
			for _, word := range words {
				if v, ok := wordStat[word]; ok {
					wordStat[word] = v + 1
				} else {
					wordStat[word] = 1
				}
			}
		}

		return wordStat, nil
	}
}

// 排序计算 倒序
type SortProcessor struct{}

func (p *SortProcessor) Process(ctx context.Context, params any) (any, error) {
	if wordStat, ok := params.(map[string]int); !ok {
		return nil, errors.New("Sort Input type error ")
	} else {
		var wordStatSlice []struct {
			cnt  int
			word string
		}

		for k, v := range wordStat {
			wordStatSlice = append(wordStatSlice, struct {
				cnt  int
				word string
			}{cnt: v, word: k})
		}

		// 倒排
		sort.Slice(wordStatSlice, func(i, j int) bool {
			return wordStatSlice[i].cnt > wordStatSlice[j].cnt
		})

		// 取TOP 3
		return wordStatSlice[:3], nil
	}
}

type IProcessor interface {
	Process(ctx context.Context, params any) (any, error)
}

type ISource interface {
	Process(ctx context.Context) (<-chan any, error)
}

type ISink interface {
	Process(ctx context.Context, params any) error
}

type ProcessorManager struct {
	source ISource
	sink   ISink
	ps     []IProcessor
}

func NewProcessorManager() *ProcessorManager {
	return &ProcessorManager{}
}

func (m *ProcessorManager) AddProcessor(processor IProcessor) {
	m.ps = append(m.ps, processor)
}

func (m *ProcessorManager) AddSource(source ISource) {
	m.source = source
}

func (m *ProcessorManager) AddSink(sink ISink) {
	m.sink = sink
}

func (m *ProcessorManager) Run(ctx context.Context) error {
	var err error

	in, err := m.source.Process(ctx)
	if err != nil {
		return err
	}

	// pipeline构建和执行
	for data := range in {
		for _, v := range m.ps {
			data, err = v.Process(ctx, data)

			// 错误集中处理，这里选择提前退出
			if err != nil {
				log.Printf("Process err %s\n", err)
				return nil
			}
		}

		err := m.sink.Process(ctx, data)
		if err != nil {
			log.Printf("Sink err %s\n", err)
			return nil
		}
	}

	return nil
}

func (m *ProcessorManager) RunN(ctx context.Context, maxCnt int) error {
	var err error

	in, err := m.source.Process(ctx)
	if err != nil {
		return err
	}

	// pipeline构建和执行
	syncProcess := func(data any) {
		for _, v := range m.ps {
			data, err = v.Process(ctx, data)

			// 错误集中处理，这里选择提前退出
			if err != nil {
				log.Printf("Process err %s\n", err)
				return
			}
		}

		err := m.sink.Process(ctx, data)
		if err != nil {
			log.Printf("Sink err %s\n", err)
			return
		}
	}

	wg := sync.WaitGroup{}
	wg.Add(maxCnt)

	// 多个协程消费同一个channel
	for i := 0; i < maxCnt; i++ {
		go func() {
			defer wg.Done()

			for data := range in {
				syncProcess(data)
			}
		}()
	}

	wg.Wait()

	return nil
}

func TestSimpleWordCount4(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	m := NewProcessorManager()

	// pipeline组装
	m.AddSource(NewTimerSource(LINES))
	m.AddSink(NewConsoleSink())

	m.AddProcessor(&SplitProcessor{})
	m.AddProcessor(&CountProcessor{})
	m.AddProcessor(&SortProcessor{})

	// 定时通知退出
	go func() {
		time.Sleep(3 * time.Second)
		cancel()
	}()

	err := m.Run(ctx)
	if err != nil {
		t.Logf("Run error %v", err.Error())
		return
	}
}

func TestSimpleWordCount5(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	m := NewProcessorManager()

	// pipeline组装
	m.AddSource(NewTimerSource(LINES))
	m.AddSink(NewConsoleSink())

	m.AddProcessor(&SplitProcessor{})
	m.AddProcessor(&CountProcessor{})
	m.AddProcessor(&SortProcessor{})

	// 定时通知退出
	go func() {
		time.Sleep(3 * time.Second)
		cancel()
	}()

	err := m.RunN(ctx, 2)
	if err != nil {
		t.Logf("Run error %v", err.Error())
		return
	}
}
