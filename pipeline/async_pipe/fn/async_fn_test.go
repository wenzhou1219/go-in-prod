package fn

import (
	"context"
	"fmt"
	"log"
	"testing"
	"time"
)

// 生成器 输入数据依次放入输出通道
func producer(ctx context.Context, nums ...int) (<-chan int, error) {
	outChannel := make(chan int)

	go func() {
		defer close(outChannel)

		for _, s := range nums {
			// 定时间隔输出
			time.Sleep(time.Second * 1)

			select {
			case outChannel <- s:
			case <-ctx.Done():
				log.Printf("producer Receive exit msg %v\n", ctx.Err())
				return
			}
		}
	}()

	return outChannel, nil
}

// 计算平方
func sq(ctx context.Context, in <-chan int) <-chan int {
	outChannel := make(chan int)

	go func() {
		defer close(outChannel)

		for s := range in {
			select {
			case outChannel <- s * s:
			case <-ctx.Done():
				log.Printf("sq Receive exit msg %v\n", ctx.Err())
				return
			}
		}
	}()

	return outChannel
}

// 累加
func sum(ctx context.Context, in <-chan int) <-chan int {
	outChannel := make(chan int)

	go func() {
		defer close(outChannel)

		var sum = 0
		for s := range in {
			sum += s

			select {
			case outChannel <- sum:
			case <-ctx.Done():
				log.Printf("sq Receive exit msg %v\n", ctx.Err())
				return
			}
		}

		outChannel <- sum
	}()

	return outChannel
}

// 输出到命令行
func sink[T any](ctx context.Context, values <-chan T) <-chan T {
	for {
		select {
		case <-ctx.Done():
			log.Printf("sink Receive exit msg %v\n", ctx.Err())
			return nil

		case val, ok := <-values:
			if ok {
				fmt.Printf("sink value: %v\n", val)
			} else {
				log.Println("sink data channel closed!")
				return nil
			}
		}
	}
}

type SourceFunc func(context.Context, ...int) (<-chan int, error)
type PipeFunc func(context.Context, <-chan int) <-chan int

func PipeProcessBuildAndRun(ctx context.Context, nums []int, sourceFunc SourceFunc, pipeFncs ...PipeFunc) (<-chan int, error) {
	ch, err := sourceFunc(ctx, nums...)
	if err != nil {
		return ch, err
	}

	for i := range pipeFncs {
		ch = pipeFncs[i](ctx, ch)
	}

	return ch, nil
}

func TestSimpleAsync(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// 定时取消
	go func() {
		time.Sleep(time.Second * 3)
		cancel()
	}()

	// 组装和执行pipeline
	var nums = []int{1, 2, 3, 4, 5}
	_, err := PipeProcessBuildAndRun(ctx, nums, producer, sq, sum, sink[int])
	if err != nil {
		return
	}
}
