package fn

import (
	"context"
	"golang.org/x/sync/semaphore"
	"log"
	"runtime"
	"sync"
	"testing"
	"time"
)

func calc(data int) int {
	// 加延迟，模拟计算时间长
	time.Sleep(2 * time.Second)

	return data * data
}

// 不限制并发度，每来一个起一个协程处理
func sqInfinitePool(ctx context.Context, in <-chan int) <-chan int {
	// merge后输出chan
	outChannel := make(chan int)

	go func() {
		defer close(outChannel)

		var wg sync.WaitGroup

		for s := range in {

			// 每个输入起协程处理
			wg.Add(1)
			go func() {
				defer wg.Done()

				select {
				case outChannel <- calc(s):
				case <-ctx.Done():
					log.Printf("sqDynamicPool Receive exit msg %v\n", ctx.Err())
					return
				}
			}()
		}

		// 全部处理完毕
		wg.Wait()
	}()

	return outChannel
}

// Bounded parallelism
// 固定并发度，共享一个输出channel 同时处理
func sqFixedPool(ctx context.Context, in <-chan int) <-chan int {
	maxWorker := 2

	// merge后输出chan
	outChannel := make(chan int)

	go func() {
		defer close(outChannel)

		var wg sync.WaitGroup
		wg.Add(maxWorker)

		// 指定个数协程运行
		for i := 0; i < maxWorker; i++ {
			go func() {
				defer wg.Done()

				for s := range in {
					select {
					case outChannel <- calc(s):
					case <-ctx.Done():
						log.Printf("sq Receive exit msg %v\n", ctx.Err())
						return
					}
				}
			}()
		}

		// 全部处理完毕
		wg.Wait()

	}()

	return outChannel
}

// 动态并发度，指定最大允许并发度，根据任务数据多少自行控制
func sqDynamicPool(ctx context.Context, in <-chan int) <-chan int {
	maxWorker := 5

	// merge后输出chan
	outChannel := make(chan int)

	go func() {
		defer close(outChannel)

		var wg sync.WaitGroup
		sm := semaphore.NewWeighted(int64(maxWorker))

		for s := range in {
			// 信号量检查，保证最大并发度限制
			sm.Acquire(ctx, 1)

			// 每个输入起协程处理
			wg.Add(1)
			go func() {
				defer sm.Release(1)
				defer wg.Done()

				select {
				case outChannel <- calc(s):
				case <-ctx.Done():
					log.Printf("sqDynamicPool Receive exit msg %v\n", ctx.Err())
					return
				}
			}()
		}

		// 全部处理完毕
		wg.Wait()
	}()

	return outChannel
}

// 可指定并发策略，借助Fanout/Fanin Split/Clone+Merge动态调配数据分发过程
func sqFanOutInPool(ctx context.Context, in <-chan int) <-chan int {
	maxWorker := 2
	splitChan := Split(in, maxWorker)

	cs := make([]chan int, 0)

	for i := 0; i < maxWorker; i++ {
		// 每个函数创建一个输出通道
		out := make(chan int)
		cs = append(cs, out)

		// 多个函数共享一个输入通道
		go func(index int) {
			defer close(out)

			for data := range splitChan[index] {
				out <- calc(data)
			}
		}(i)
	}

	return Merge(cs)
}

// runNumGoroutineMonitor 协程数量监控
func runNumGoroutineMonitor() {
	log.Printf("协程数量->%d\n", runtime.NumGoroutine())

	for {
		select {
		case <-time.After(time.Second):
			log.Printf("协程数量->%d\n", runtime.NumGoroutine())
		}
	}
}

func TestFanAsync(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go runNumGoroutineMonitor()

	// 定时取消
	go func() {
		time.Sleep(time.Second * 15)
		cancel()
	}()

	// 组装和执行pipeline
	var nums = []int{1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5}
	_, err := PipeProcessBuildAndRun(ctx, nums, producer, sqFanOutInPool, sink[int])
	if err != nil {
		return
	}
}
