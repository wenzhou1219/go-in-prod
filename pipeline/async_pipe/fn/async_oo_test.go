package fn

import (
	"context"
	"errors"
	"fmt"
	"log"
	"sync"
	"testing"
	"time"
)

type IProcessor interface {
	Process(ctx context.Context, wg *sync.WaitGroup, dataChan <-chan int, errChan chan error) <-chan int
}

type ISource interface {
	Process(ctx context.Context, wg *sync.WaitGroup, errChan chan error) <-chan int
}

type ISink interface {
	Process(ctx context.Context, wg *sync.WaitGroup, dataChan <-chan int, errChan chan error)
}

type IError interface {
	Process(ctx context.Context, wg *sync.WaitGroup, errChan chan error, cancel context.CancelFunc)
}

type ProcessorManager struct {
	source  ISource
	sink    ISink
	err     IError
	ps      []IProcessor
	errChan chan error
}

func NewProcessorManager() *ProcessorManager {
	return &ProcessorManager{errChan: make(chan error, 1)}
}

func (m *ProcessorManager) AddProcessor(processor IProcessor) {
	m.ps = append(m.ps, processor)
}

func (m *ProcessorManager) AddSource(source ISource) {
	m.source = source
}

func (m *ProcessorManager) AddSink(sink ISink) {
	m.sink = sink
}

func (m *ProcessorManager) AddError(err IError) {
	m.err = err
}

func (m *ProcessorManager) Run(ctx context.Context) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var wg = sync.WaitGroup{}

	// 组装pipeline
	wg.Add(1)
	dataChan := m.source.Process(ctx, &wg, m.errChan)

	for _, v := range m.ps {
		wg.Add(1)
		dataChan = v.Process(ctx, &wg, dataChan, m.errChan)
	}

	wg.Add(1)
	m.sink.Process(ctx, &wg, dataChan, m.errChan)

	go func() {
		wg.Wait()
		close(m.errChan)
	}()

	// 错误通道阻塞，错误处理集中处理
	// 出现错误则通知退出，可灵活定制处理策略
	m.err.Process(ctx, &wg, m.errChan, cancel)
}

// 生成器 输入数据依次放入输出通道
type TimerSource struct {
	Nums []int
}

func NewTimerSource(nums ...int) *TimerSource {
	return &TimerSource{Nums: nums}
}

func (t *TimerSource) Process(ctx context.Context, wg *sync.WaitGroup, errChan chan error) <-chan int {
	defer wg.Done()

	outChannel := make(chan int)

	go func() {
		defer close(outChannel)

		i := 0

		for {
			select {
			// 定时输出
			case <-time.After(1 * time.Second):
				if i >= len(t.Nums) {
					return
				}

				s := t.Nums[i]
				i = i + 1
				if s < 0 {
					errChan <- errors.New("Invalid Num")
					continue
				}

				outChannel <- s

			// 外部中断
			case <-ctx.Done():
				log.Println("Notify to exit source!!!")
				return
			}
		}
	}()

	return outChannel
}

// 计算平方
type SqProcessor struct {
}

func (s *SqProcessor) Process(ctx context.Context, wg *sync.WaitGroup, dataChan <-chan int, errChan chan error) <-chan int {
	defer wg.Done()
	outChannel := make(chan int)

	go func() {
		defer close(outChannel)

		for {
			select {
			case s, ok := <-dataChan:
				if !ok {
					log.Println("sq data channel closed!")
					return
				}

				outChannel <- s * s
			}
		}
	}()

	return outChannel
}

// 累加
type SumProcessor struct {
}

func (s *SumProcessor) Process(ctx context.Context, wg *sync.WaitGroup, dataChan <-chan int, errChan chan error) <-chan int {
	defer wg.Done()
	outChannel := make(chan int)

	go func() {
		defer close(outChannel)
		var sum = 0

		for {
			select {
			case s, ok := <-dataChan:
				if !ok {
					log.Println("sum data channel closed!")
					return
				}

				sum += s
				outChannel <- sum
			}
		}
	}()

	return outChannel
}

// 输出到命令行
type ConsoleSink struct {
}

func NewConsoleSink() *ConsoleSink {
	return &ConsoleSink{}
}

func (s *ConsoleSink) Process(ctx context.Context, wg *sync.WaitGroup, dataChan <-chan int, errChan chan error) {

	go func() {
		defer wg.Done()

		for {
			select {
			case val, ok := <-dataChan:
				if ok {
					fmt.Printf("sink value: %v\n", val)
				} else {
					log.Println("sink data channel closed!")
					return
				}
			}
		}
	}()
}

// 错误处理
type ErrorPolicyExit struct {
}

func NewErrorPolicyExit() *ErrorPolicyExit {
	return &ErrorPolicyExit{}
}

func (p *ErrorPolicyExit) Process(ctx context.Context, wg *sync.WaitGroup, errChan chan error, cancel context.CancelFunc) {
	for {
		select {
		case err, ok := <-errChan:
			if !ok {
				log.Println("error channel closed and exit!")
				return
			}

			log.Printf("Receive error %v\n", err)
			cancel()
		}
	}
}

func TestProcessorManager(t *testing.T) {
	m := NewProcessorManager()

	// pipeline组装
	//m.AddSource(NewTimerSource(1, 2, 3, 4, 5))
	m.AddSource(NewTimerSource(1, 2, -1, 3, 4, 5))
	m.AddSink(NewConsoleSink())

	m.AddError(NewErrorPolicyExit())

	m.AddProcessor(&SqProcessor{})
	m.AddProcessor(&SumProcessor{})

	// 执行
	m.Run(context.Background())
}
