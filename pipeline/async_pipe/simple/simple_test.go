package simple

import (
	"fmt"
	"testing"
)

// 生成器 输入数据依次放入输出通道
func producer(nums ...int) <-chan int {
	outChannel := make(chan int)

	go func() {
		defer close(outChannel)
		for _, n := range nums {
			outChannel <- n
		}
	}()

	return outChannel
}

// 计算平方
func sq(in <-chan int) <-chan int {
	outChannel := make(chan int)

	go func() {
		defer close(outChannel)
		for n := range in {
			outChannel <- n * n
		}
	}()

	return outChannel
}

// 累加
func sum(in <-chan int) <-chan int {
	outChannel := make(chan int)

	go func() {
		defer close(outChannel)

		var sum = 0
		for n := range in {
			sum += n
		}

		outChannel <- sum
	}()

	return outChannel
}

// 输出到命令行
func sink(in <-chan int) <-chan int {
	for val := range in {
		fmt.Printf("sink value: %v\n", val)
	}

	return nil
}

func TestSimpleAsync(t *testing.T) {
	sink(sum(sq(producer(1, 2, 3, 4, 5))))
}

type SourceFunc func(...int) <-chan int
type PipeFunc func(<-chan int) <-chan int

func PipeProcessBuildAndRun(nums []int, sourceFnc SourceFunc, pipeFncs ...PipeFunc) <-chan int {
	ch := sourceFnc(nums...)

	// 依次执行函数，每个函数消费上一个函数返回channel
	for i := range pipeFncs {
		ch = pipeFncs[i](ch)
	}

	return ch
}

func TestSimpleAsync2(t *testing.T) {

	var nums = []int{1, 2, 3, 4, 5}
	PipeProcessBuildAndRun(nums, producer, sq, sum, sink)
}
