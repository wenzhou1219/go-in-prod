package main

import (
	"fmt"
	"github.com/spf13/pflag"
)

type Options2 struct {
	Verbose bool

	// 指定必须(不支持)
	Name string

	// 指定默认值
	Age int

	// 指定help显示字段名(不支持)
	File string

	// 限制取值范围在指定范围内(不支持)
	Animal string

	// 映射成list文本
	StringSlice []string

	// 映射成map
	IntMap map[string]int
}

func main() {
	var opts Options2

	pflag.BoolVarP(&opts.Verbose, "verbose", "v", false, "Show verbose debug information")
	pflag.StringVarP(&opts.Name, "name", "n", "", "A name")
	pflag.IntVarP(&opts.Age, "age", "a", 10, "A Age")
	pflag.StringVarP(&opts.File, "file", "f", "", "A file")
	pflag.StringVar(&opts.Animal, "animal", "f", "animal")

	pflag.StringSliceVarP(&opts.StringSlice, "slice", "s", nil, "A slice of strings")
	pflag.StringToIntVarP(&opts.IntMap, "intmap", "i", nil, "A map from string to int")

	pflag.Parse()
	fmt.Printf("Value:%v\n", opts)
}
