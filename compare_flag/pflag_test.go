package compare_flag

import (
	"fmt"
	"github.com/spf13/pflag"
	"testing"
)

type Options2 struct {
	Verbose bool

	// 指定必须(不支持)
	Name string

	// 指定默认值
	Age int

	// 指定help显示字段名(不支持)
	File string

	// 限制取值范围在指定范围内(不支持)
	Animal string

	// 映射成list文本
	StringSlice []string

	// 映射成map
	IntMap map[string]int
}

func TestSimplePFlags(t *testing.T) {
	var opts Options2

	pflag.BoolVarP(&opts.Verbose, "verbose", "v", false, "Show verbose debug information")
	pflag.StringVarP(&opts.Name, "name", "n", "", "A name")
	pflag.IntVarP(&opts.Age, "age", "a", 10, "A Age")
	pflag.StringVarP(&opts.File, "file", "f", "", "A file")
	pflag.StringVar(&opts.Animal, "animal", "f", "animal")

	pflag.StringSliceVarP(&opts.StringSlice, "slice", "s", nil, "A slice of strings")
	pflag.StringToIntVarP(&opts.IntMap, "intmap", "i", nil, "A map from string to int")

	// -h时打印help信息
	args := []string{
		"-h",
	}

	//pflag.CommandLine.Parse(args)

	// 输入参数测试
	args = []string{
		"-v",
		"-n", "Me",
		"--age=5",
		"-f", "config.xml",
		"--animal", "dog", // anything other than "cat" or "dog" will raise an error
		"-s", "hello,xxx",
		"-s", "world",
		"--intmap", "a=1",
		"--intmap", "b=5",
	}

	//pflag.Parse()
	pflag.CommandLine.Parse(args)

	fmt.Printf("Value:%v", opts)
}

func TestSimplePFlagSet(t *testing.T) {
	flagSet := pflag.NewFlagSet("test", pflag.ContinueOnError)

	var opts Options2

	flagSet.BoolVarP(&opts.Verbose, "verbose", "v", false, "Show verbose debug information")
	flagSet.StringVarP(&opts.Name, "name", "n", "", "A name")
	flagSet.IntVarP(&opts.Age, "age", "a", 10, "A Age")
	flagSet.StringVarP(&opts.File, "file", "f", "", "A file")
	flagSet.StringVar(&opts.Animal, "animal", "f", "animal")

	flagSet.StringSliceVarP(&opts.StringSlice, "slice", "s", nil, "A slice of strings")
	flagSet.StringToIntVarP(&opts.IntMap, "intmap", "i", nil, "A map from string to int")

	// -h时打印help信息
	args := []string{
		"-h",
	}

	//flagSet.Parse(args)

	// 输入参数测试
	args = []string{
		"-v",
		"-n", "Me",
		"--age=5",
		"-f", "config.xml",
		"--animal", "dog", // anything other than "cat" or "dog" will raise an error
		"-s", "hello,xxx",
		"-s", "world",
		"--intmap", "a=1",
		"--intmap", "b=5",
	}

	//pflag.Parse()
	flagSet.Parse(args)

	fmt.Printf("Value:%v", opts)
}
