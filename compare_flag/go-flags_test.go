package compare_flag

import (
	"fmt"
	"github.com/jessevdk/go-flags"
	"testing"
)

// https://pkg.go.dev/github.com/jessevdk/go-flags#section-readme
type Options struct {
	// 最常用 short-短名称 long-长名称 description-命令行描述
	Verbose bool `short:"v" long:"verbose" description:"Show verbose debug information"`

	// required-指定必须，默认不是必须
	Name string `short:"n" long:"name" description:"A name" required:"true"`

	// default-指定默认值 default-mask指定显示help文本中的默认值描述
	Age int `short:"a" long:"age" description:"A Age" default:"10" default-mask:"10"`

	// value-name指定help显示字段名
	File string `short:"f" long:"file" description:"A file" value-name:"FILE"`

	// choice-限制取值范围在指定范围内
	Animal string `long:"animal" choice:"cat" choice:"dog"`

	// 映射成list文本
	StringSlice []string `short:"s" description:"A slice of strings"`

	// 映射成map
	IntMap map[string]int `long:"intmap" description:"A map from string to int"`
}

func TestSimpleFlags(t *testing.T) {
	// -h时打印help信息
	args := []string{
		"-h",
	}

	var opts Options
	flags.ParseArgs(&opts, args)

	// 输入参数测试
	args = []string{
		"-v",
		"-n", "Me",
		"--age=5",
		"-f", "config.xml",
		"--animal", "dog", // anything other than "cat" or "dog" will raise an error
		"-s", "hello,xxx",
		"-s", "world",
		"--intmap", "a:1",
		"--intmap", "b:5",
	}

	_, err := flags.ParseArgs(&opts, args)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Value:%v", opts)
}
