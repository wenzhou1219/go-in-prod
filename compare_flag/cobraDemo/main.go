/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package main

import "go-in-prod/flag_compare/cobraDemo/cmd"

// 参考 https://www.cnblogs.com/jiujuan/p/15487918.html

func main() {
	cmd.Execute()
}
