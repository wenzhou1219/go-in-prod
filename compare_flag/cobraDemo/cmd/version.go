/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	version bool
)

// Cmd
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "version命令简要描述",
	Long:  `version命令详细描述，支持换行，执行程序默认输出该信息 ...`,
	// 实现如下函数响应
	Run: func(cmd *cobra.Command, args []string) {
		if version {
			fmt.Println("输出版本信息")
		}
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)

	// 定义 命令行flags解析(Pflag) 和 configuration配置文件解析(Viper).
	// Cobra 定义 persistent flags，对应选项对所有子命令生效

	// Cobra 定义 local flags, 只对当前子命令生效
	versionCmd.Flags().BoolVarP(&version, "version", "v", false, "Help message for version")
}
