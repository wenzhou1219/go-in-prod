/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
)

var (
	cfgFile string
)

// rootCmd
// 根命令，没有子命令时调用
var rootCmd = &cobra.Command{
	Use:   "cobraDemo",
	Short: "当前程序简要描述",
	Long:  `当前程序详细描述，支持换行，执行程序默认输出该信息 ...`,
	// 根命令如需添加响应和默认输出help，实现如下函数响应
	Run: func(cmd *cobra.Command, args []string) {},
}

// Execute
// 添加子命令，main.main()中初始化调用一次即可
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// 定义 命令行flags解析(Pflag) 和 configuration配置文件解析(Viper).
	// cobra.OnInitialize(initConfig)

	// Cobra 定义 persistent flags，对应选项对所有子命令生效
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.cobraDemo.yaml)")

	// Cobra 定义 local flags, 只对当前子命令生效
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	// viper 和flag绑定
	// ...
}

func initConfig() {
	// == 设置配置文件路径
	// 从cfgFile指定或home目录查找
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// 指定后缀查找.cobra
		viper.AddConfigPath(home)
		viper.SetConfigName(".cobra")
	}

	// == 读取配置文件
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("Can't read config:", err)
		os.Exit(1)
	}
}
