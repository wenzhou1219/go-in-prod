/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	stat bool
)

// Cmd
var statCmd = &cobra.Command{
	Use:   "stat",
	Short: "stat命令简要描述",
	Long:  `stat命令详细描述，支持换行，执行程序默认输出该信息 ...`,
	// 实现如下函数响应
	Run: func(cmd *cobra.Command, args []string) {
		if stat {
			fmt.Println("输出统计信息")
		}
	},
}

func init() {
	rootCmd.AddCommand(statCmd)

	// 定义 命令行flags解析(Pflag) 和 configuration配置文件解析(Viper).
	// Cobra 定义 persistent flags，对应选项对所有子命令生效

	// Cobra 定义 local flags, 只对当前子命令生效
	statCmd.Flags().BoolVarP(&stat, "stat", "s", false, "Help message for stat")
	cobra.MarkFlagRequired(statCmd.Flags(), "stat")
}
