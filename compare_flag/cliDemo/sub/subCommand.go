package sub

import (
	"fmt"
	"github.com/urfave/cli"
)

var (
	taskInfo string
)

func GetSubCommands() []cli.Command {
	return []cli.Command{
		{
			Name:    "add",
			Aliases: []string{"a"},
			Usage:   "add a task to the list",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:        "t, taskinfo",
					Value:       "default info",
					Usage:       "task info",
					Destination: &taskInfo,
				},
			},
			Action: func(cCtx *cli.Context) error {
				fmt.Printf("added task:%s info:%s\n", cCtx.Args().First(), taskInfo)
				return nil
			},
		},
		{
			Name:    "complete",
			Aliases: []string{"c"},
			Usage:   "complete a task on the list",
			Action: func(cCtx *cli.Context) error {
				fmt.Println("completed task: ", cCtx.Args().First())
				return nil
			},
		},
		{
			Name:    "template",
			Aliases: []string{"t"},
			Usage:   "options for task templates",
			Subcommands: []cli.Command{
				{
					Name:  "add",
					Usage: "add a new template",
					Action: func(cCtx *cli.Context) error {
						fmt.Println("new task template: ", cCtx.Args().First())
						return nil
					},
				},
				{
					Name:  "remove",
					Usage: "remove an existing template",
					Action: func(cCtx *cli.Context) error {
						fmt.Println("removed task template: ", cCtx.Args().First())
						return nil
					},
				},
			},
		},
	}
}
