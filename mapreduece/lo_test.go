package mapreduece

import (
	"fmt"
	"github.com/samber/lo"
	lop "github.com/samber/lo/parallel"
	"testing"
)

func TestName(t *testing.T) {
	s := lo.Range(1000)

	m := lop.Map[int, int](s, func(item int, index int) int {
		return item * item
	})

	r := lo.Sum(m)

	fmt.Println(r)
}
