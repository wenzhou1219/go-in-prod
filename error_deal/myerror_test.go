package error_deal

import (
	"fmt"
	"testing"
)

// 单独保存的业务error code和msg等

const (
	ErrSuccess   = 0
	ErrInvalid   = 1
	ErrWrongUser = 2
)

var code2msg = map[int]string{
	ErrSuccess:   "Success",
	ErrInvalid:   "Invalid Param",
	ErrWrongUser: "Wrong User",
}

// 独立的自定义error
type MyError struct {
	mod  string
	code int
	msg  string
}

func NewMyError(mod string, code int) *MyError {
	return &MyError{
		mod,
		code,
		code2msg[code],
	}
}

func (e *MyError) Error() string {
	return fmt.Sprintf("Error detail: %+v %+v %+v", e.mod, e.code, e.msg)
}

func (e *MyError) Code() int {
	return e.code
}

func (e *MyError) Msg() string {
	return e.msg
}

func doSomethingA() error {
	return NewMyError("A", ErrSuccess)
}

func doSomethingB() error {
	return NewMyError("B", ErrInvalid)
}

func TestDoSomething(t *testing.T) {
	err := doSomethingA()

	if err != nil {
		// 分错误处理
		switch err.(type) {
		case *MyError:

			// 分业务处理
			myerr := err.(*MyError)
			switch myerr.Code() {
			case ErrSuccess:
				// ...
				t.Logf("MyError - %v", err)
			case ErrInvalid:
				// ...
			case ErrWrongUser:
				// ...
			}

		default:
			t.Log("Other error")
		}
	}
}
