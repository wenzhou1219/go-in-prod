package error_deal

import (
	"errors"
	"fmt"
	giterrors "github.com/pkg/errors"
	"testing"
)

var ErrDbOrigin2 = errors.New("ErrDbOrigin")

type ErrDbDefine2 struct {
	info string
}

func (e ErrDbDefine2) Error() string {
	return fmt.Sprintf("Error detail: %+v ", e.info)
}

func controlFunc2(param interface{}) error {
	if err := serviceFunc2(param); err != nil {
		return giterrors.Wrap(err, "error when controlFunc")
	}

	return nil
}

func serviceFunc2(param interface{}) error {
	if err := daoFunc2(param); err != nil {
		return giterrors.Wrap(err, "error when serviceFunc")
	}

	return nil
}

func daoFunc2(param interface{}) error {
	if err := dbFunc2(param); err != nil {
		return giterrors.Wrap(err, "error when daoFunc")
	}

	return nil
}

func dbFunc2(param interface{}) error {
	// do something
	return ErrDbOrigin2
	//return ErrDbDefine2{"ErrDbOrigin error info"}
}

func TestWrapError2(t *testing.T) {
	param := 0

	if err := controlFunc2(param); err != nil {
		fmt.Printf("error -> %v\n", err)
		fmt.Printf("error unwrap -> %v\n", giterrors.Unwrap(err))

		fmt.Println(giterrors.WithStack(err))

		// 无法匹配
		if err == ErrDbOrigin2 {
			fmt.Printf("error print with equal -> %v\n", err)
		}

		if giterrors.Cause(err) == ErrDbOrigin2 {
			fmt.Printf("error print with Cause -> %v\n", err)
		}

		if giterrors.Is(err, ErrDbOrigin2) {
			fmt.Printf("error print with Is -> %v\n", err)
		}

		// 无法匹配
		if v, ok := err.(ErrDbDefine2); ok {
			fmt.Printf("error print with type -> %v\n", v)
		}

		var b ErrDbDefine2
		if giterrors.As(err, &b) {
			fmt.Printf("error print with As -> %v\n", err)
		}

	}
}
