package error_deal

import (
	"encoding/binary"
	"errors"
	"io"
	"testing"
)

// 简单处理
func funcAA() error {
	// do something
	return errors.New("funcA error")
}

func funcBB() error {
	// do something
	return errors.New("funcB error")
}

func funcCC() error {
	// do something
	return errors.New("funcC error")
}

func TestSimpleTidy(t *testing.T) {

	// 外包函数判断
	var err error
	callFunc := func(f func() error) {
		if err != nil {
			return
		}

		err = f()
	}

	// 顺序调用
	callFunc(funcAA)
	callFunc(funcBB)
	callFunc(funcCC)

	// 统一判断
	if err != nil {
		t.Logf("Error - %v", err)
	}

}

type WorkRunner struct {
	err error
}

func NewWorkRunner() *WorkRunner {
	return &WorkRunner{}
}

func (w *WorkRunner) run(f func() error) {
	if w.err == nil {
		w.err = f()
	}
}

func (w *WorkRunner) funcAA() *WorkRunner {
	// do something
	w.run(func() error {
		return errors.New("funcA error")
	})
	return w
}

func (w *WorkRunner) funcBB() *WorkRunner {
	// do something
	w.run(func() error {
		return errors.New("funcB error")
	})
	return w
}

func (w *WorkRunner) funcCC() *WorkRunner {
	// do something
	w.run(func() error {
		return errors.New("funcC error")
	})
	return w
}

func TestSimpleTidy2(t *testing.T) {

	// 对象统一管理判断
	w := NewWorkRunner()

	// 顺序链式调用
	w.funcAA().funcBB().funcCC()

	// 统一判断
	if w.err != nil {
		t.Logf("Error - %v", w.err)
	}
}

type Reader struct {
	r   io.Reader
	err error
}

func (r *Reader) read(data interface{}) {
	if r.err == nil {
		r.err = binary.Read(r.r, binary.BigEndian, data)
	}
}
