package error_deal

import (
	"errors"
	"fmt"
	"testing"
)

var ErrDbOrigin = errors.New("ErrDbOrigin")

type ErrDbDefine struct {
	info string
}

func (e ErrDbDefine) Error() string {
	return fmt.Sprintf("Error detail: %+v ", e.info)
}

func controlFunc(param interface{}) error {
	if err := serviceFunc(param); err != nil {
		return fmt.Errorf("error when controlFunc...: [%w]", err)
	}

	return nil
}

func serviceFunc(param interface{}) error {
	if err := daoFunc(param); err != nil {
		return fmt.Errorf("error when serviceFunc...: [%w]", err)
	}

	return nil
}

func daoFunc(param interface{}) error {
	if err := dbFunc(param); err != nil {
		return fmt.Errorf("error when daoFunc...: [%w]", err)
	}

	return nil
}

func dbFunc(param interface{}) error {
	// do something
	return ErrDbOrigin
	//return ErrDbDefine{"ErrDbOrigin error info"}
}

func TestWrapError(t *testing.T) {
	param := 0

	if err := controlFunc(param); err != nil {
		fmt.Printf("error -> %v\n", err)
		fmt.Printf("error unwrap -> %v\n", errors.Unwrap(err))

		// 无法匹配
		if err == ErrDbOrigin {
			fmt.Printf("error print with equal -> %v\n", err)
		}

		if errors.Is(err, ErrDbOrigin) {
			fmt.Printf("error print with Is -> %v\n", err)
		}

		// 无法匹配
		if v, ok := err.(ErrDbDefine); ok {
			fmt.Printf("error print with type -> %v\n", v)
		}

		var b ErrDbDefine
		if errors.As(err, &b) {
			fmt.Printf("error print with As -> %v\n", err)
		}

	}
}
