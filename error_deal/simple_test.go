package error_deal

import (
	"errors"
	"fmt"
	"testing"
)

// 简单处理
func funcA() error {
	// do something
	return errors.New("funcA error")
}

func funcB() error {
	// do something
	return fmt.Errorf("funcB error %d", 1)
}

func funcC() error {
	err := funcB()
	if err != nil {
		return fmt.Errorf("funcC error %w", err)
	}

	// do something

	return nil
}

func TestSimple(t *testing.T) {
	err := funcA()
	if err != nil {
		t.Logf("err %v", err)
		return
	}

	err = funcB()
	if err != nil {
		t.Logf("err %v", err)
		return
	}

	err = funcC()
	if err != nil {
		t.Logf("err %v", err)
		return
	}
}

// 分支判断
var (
	ErrA = errors.New("A error")
	ErrB = errors.New("B error")
	ErrC = errors.New("C error")
)

func funcA2() error {
	// do something
	if true {
		return ErrA
	}

	// do something
	return nil
}

func funcB2(param int) error {
	if param == 0 {
		return ErrA
	} else if param == 1 {
		return ErrB
	} else if param == 2 {
		return ErrC
	}

	// do something
	return nil
}

func TestSimple2(t *testing.T) {
	err := funcA2()
	if err == ErrA {
		t.Logf("err %v", err)
		return
	}

	err = funcB2(0)
	if err != nil {
		switch err {
		case ErrA:
			t.Logf("err A %v", err)
			//..
			return
		case ErrB:
			t.Logf("err B %v", err)
			//...
			return
		case ErrB:
			t.Logf("err B %v", err)
			//..
			return
		default:
			//...
			return
		}
	}
}

// 自定义错误判断
type ErrMyA struct {
	Param string
}

func (e *ErrMyA) Error() string {
	return fmt.Sprintf("invalid param: %+v", e.Param)
}

type ErrMyB struct {
	Param string
}

func (e *ErrMyB) Error() string {
	return fmt.Sprintf("invalid param: %+v", e.Param)
}

func funcB3(param int) error {
	if param == 0 {
		return &ErrMyA{"A"}
	} else if param == 1 {
		return &ErrMyB{"B"}
	}

	// do something
	return nil
}

func TestSimple3(t *testing.T) {
	err := funcB3(0)
	if v, ok := err.(*ErrMyA); ok {
		t.Logf("err %v", v)
		return
	}

	err = funcB3(0)
	if err != nil {
		switch err.(type) {
		case *ErrMyA:
			t.Logf("err A %v", err)
			//..
			return
		case *ErrMyB:
			t.Logf("err B %v", err)
			//...
			return
		default:
			//...
			return
		}
	}
}
