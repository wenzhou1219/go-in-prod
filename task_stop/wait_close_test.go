package task_stop

import (
	"sync"
	"testing"
	"time"
)

// 多个任务并行控制，等待所有任务完成
func TestTaskControl(t *testing.T) {
	dataChan := make(chan int)

	taskNum := 3

	wg := sync.WaitGroup{}
	wg.Add(taskNum)

	// 起多个协程，data关闭时退出
	for i := 0; i < taskNum; i++ {
		go func(taskNo int) {
			defer wg.Done()
			t.Logf("Task %d run\n", taskNo)

			for {
				select {
				case _, ok := <-dataChan:
					if !ok {
						t.Logf("Task %d notify to stop\n", taskNo)
						return
					}
				}
			}
		}(i)
	}

	// 通知退出
	go func() {
		time.Sleep(3 * time.Second)
		close(dataChan)
	}()

	// 等待退出完成
	wg.Wait()
}

// 多个任务并行控制，等待所有任务完成
func TestTaskControl2(t *testing.T) {
	dataChan := make(chan int)

	// 起协程返回新chan，在输出chan等待判断完成
	out := make(chan int)
	go func() {
		defer close(out) // 结束则自动关闭

		for {
			select {
			case _, ok := <-dataChan:
				if !ok {
					t.Logf("Task notify to stop\n")
					return
				}
			}
		}
	}()

	// 通知退出
	go func() {
		time.Sleep(3 * time.Second)
		close(dataChan)
	}()

	dataChan <- 1

	// 等待退出完成
	for data := range out {
		t.Logf("%d\n", data)
	}
}

func TestTaskControl3(t *testing.T) {
	dataChan := make(chan int)

	// 起协程返回新chan
	out := make(chan int)
	go func() {
		defer close(out) // 结束则自动关闭

		for {
			select {
			case _, ok := <-dataChan:
				if !ok {
					t.Logf("Task notify to stop\n")
					return
				}
			}
		}
	}()

	// 通知退出
	go func() {
		time.Sleep(3 * time.Second)
		close(dataChan)
	}()

	dataChan <- 1

	// 等待足够长时间，退出完成
	time.Sleep(10 * time.Second)
}
