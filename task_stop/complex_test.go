package task_stop

import (
	"context"
	"log"
	"sync"
	"testing"
	"time"
)

type TaskFunc func(ctx context.Context)

func runTaskFunc(wg *sync.WaitGroup, ctx context.Context, taskName string, f TaskFunc) {
	defer wg.Done()

	log.Printf("Task %s start!\n", taskName)
	f(ctx)

	for {
		select {

		case <-ctx.Done():
			log.Printf("Task %s  revice exit signal!\n", taskName)
			return
		}
	}

}

// 简单并行任务-同时停止
func TestStop(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	var wg = sync.WaitGroup{}

	// 起多个任务
	wg.Add(1)
	go runTaskFunc(&wg, ctx, "A", func(ctx context.Context) {
		wg.Add(1)
		go runTaskFunc(&wg, ctx, "B", func(ctx context.Context) {
			wg.Add(1)
			go runTaskFunc(&wg, ctx, "C", func(ctx context.Context) {
				wg.Add(1)
				go runTaskFunc(&wg, ctx, "D", func(ctx context.Context) {})
			})
		})

		wg.Add(1)
		go runTaskFunc(&wg, ctx, "E", func(ctx context.Context) {
			wg.Add(1)
			go runTaskFunc(&wg, ctx, "F", func(ctx context.Context) {
				wg.Add(1)
				go runTaskFunc(&wg, ctx, "G", func(ctx context.Context) {})
			})
		})
	})

	// 通知关闭
	go func() {
		time.Sleep(3 * time.Second)
		cancel()
	}()

	// 等待全部关闭后退出
	wg.Wait()
}

// 简单并行任务-控制停止顺序
func TestStop2(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	ctxb, cancelb := context.WithCancel(ctx)
	ctxe, cancele := context.WithCancel(ctx)

	var wg = sync.WaitGroup{}

	// 起多个任务
	wg.Add(1)
	go runTaskFunc(&wg, ctx, "A", func(ctx context.Context) {
		wg.Add(1)
		go runTaskFunc(&wg, ctxb, "B", func(ctx context.Context) {
			wg.Add(1)
			go runTaskFunc(&wg, ctx, "C", func(ctx context.Context) {
				wg.Add(1)
				go runTaskFunc(&wg, ctx, "D", func(ctx context.Context) {})
			})
		})

		wg.Add(1)
		go runTaskFunc(&wg, ctxe, "E", func(ctx context.Context) {
			wg.Add(1)
			go runTaskFunc(&wg, ctx, "F", func(ctx context.Context) {
				wg.Add(1)
				go runTaskFunc(&wg, ctx, "G", func(ctx context.Context) {})
			})
		})
	})

	// 通知关闭
	go func() {
		time.Sleep(3 * time.Second)
		cancele()
		time.Sleep(3 * time.Second)
		cancelb()
		time.Sleep(3 * time.Second)
		cancel()
	}()

	// 等待全部关闭后退出
	wg.Wait()
}

// 数据流转任务-逐级退出
func execDataTaskFunc(ctx context.Context, dataChan chan int, taskName string) chan int {
	//out := make(chan int)
	out := make(chan int, 100)

	log.Printf("Task %s start!\n", taskName)

	go func() {
		defer close(out)

		for {
			select {
			case data, ok := <-dataChan:
				if !ok {
					log.Printf("Task %s  revice data channel close signal!\n", taskName)
					return
				}

				time.Sleep(2 * time.Second)
				out <- data
			case <-ctx.Done():
				log.Printf("Task %s  revice exit signal!\n", taskName)
				return
			}
		}
	}()

	return out
}

func TestDataTaskStop(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	dataChanInput := make(chan int)

	// 嵌套运行协程
	taskChanA := execDataTaskFunc(ctx, dataChanInput, "A")
	taskChanB := execDataTaskFunc(ctx, taskChanA, "B")
	taskChanC := execDataTaskFunc(ctx, taskChanB, "C")

	// 通知退出
	go func() {
		i := 0
		for {
			select {
			case <-time.After(time.Second):
				i = i + 1
				if i == 10 {
					t.Logf("Notify to stop!!!")
					close(dataChanInput)
					//cancel()
					return
				}

				dataChanInput <- i
			}
		}
	}()

	//  等待退出
	for data := range taskChanC {
		t.Logf("Out->%d", data)
	}
}
