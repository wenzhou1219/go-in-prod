package task_stop

import (
	"context"
	"log"
	"sync"
	"testing"
	"time"
)

// cancelFn 数据通道关闭通知退出
func cancelFn(dataChan chan int) {
	for {
		select {
		case val, ok := <-dataChan:
			// 关闭data通道时，通知退出
			// 一个可选是判断data=指定值时退出
			if !ok {
				log.Printf("Channel closed ！！！")
				return
			}

			log.Printf("Revice dataChan %d\n", val)
		}
	}
}

func TestCancelControl(t *testing.T) {
	dataChan := make(chan int, 10)

	go cancelFn(dataChan)

	go func() {
		dataChan <- 1
		time.Sleep(1 * time.Second)
		dataChan <- 2

		close(dataChan)
	}()

	time.Sleep(10 * time.Second)
}

// exitChannelFn 单独退出通道关闭通知退出
func exitChannelFn(wg *sync.WaitGroup, taskNo int, dataChan chan int, exitChan chan struct{}) {
	defer wg.Done()

	for {
		select {
		case val, ok := <-dataChan:
			if !ok {
				log.Printf("Task %d channel closed ！！！", taskNo)
				return
			}

			log.Printf("Task %d  revice dataChan %d\n", taskNo, val)

			// 关闭exit通道时，通知退出
		case <-exitChan:
			log.Printf("Task %d  revice exitChan signal!\n", taskNo)
			return
		}
	}

}

func TestExitChannelControl(t *testing.T) {
	dataChan := make(chan int, 10)
	defer close(dataChan)
	exitChan := make(chan struct{})

	taskNum := 3

	wg := sync.WaitGroup{}
	wg.Add(taskNum)

	for i := 0; i < taskNum; i++ {
		go exitChannelFn(&wg, i, dataChan, exitChan)
	}

	go func() {
		dataChan <- 1
		dataChan <- 2
		dataChan <- 3
		time.Sleep(1 * time.Second)
		dataChan <- 4
		dataChan <- 5
		dataChan <- 6

		close(exitChan)
	}()

	wg.Wait()
}

// contextCancelFn context取消或超时通知退出
func contextCancelFn(wg *sync.WaitGroup, taskNo int, dataChan chan int, ctx context.Context) {
	defer wg.Done()

	for {
		select {
		case val, ok := <-dataChan:
			if !ok {
				log.Printf("Task %d channel closed ！！！", taskNo)
				return
			}

			log.Printf("Task %d  revice dataChan %d\n", taskNo, val)

		// ctx取消或超时，通知退出
		case <-ctx.Done():
			log.Printf("Task %d  revice exit signal!\n", taskNo)
			return
		}
	}

}

func TestContextCancelControl(t *testing.T) {
	dataChan := make(chan int, 10)
	defer close(dataChan)

	ctx, cancel := context.WithCancel(context.Background())

	taskNum := 3

	wg := sync.WaitGroup{}
	wg.Add(taskNum)

	for i := 0; i < taskNum; i++ {
		go contextCancelFn(&wg, i, dataChan, ctx)
	}

	go func() {
		dataChan <- 1
		dataChan <- 2
		dataChan <- 3
		time.Sleep(1 * time.Second)
		dataChan <- 4
		dataChan <- 5
		dataChan <- 6

		cancel()
	}()

	wg.Wait()
}
