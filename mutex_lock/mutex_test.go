package mutex_lock

import (
	"log"
	"sync"
	"testing"
)

type Calc struct {
	m sync.Mutex
}

func NewCalc() *Calc {
	return &Calc{sync.Mutex{}}
}

func (c *Calc) Do(wg *sync.WaitGroup) {
	defer wg.Done()

	c.m.Lock()

	log.Println("Do start")
	c.detailDo()
	log.Println("Do end")

	c.m.Unlock()
}

func (c *Calc) detailDo() {
	c.m.Lock()

	log.Println("detailDo start")

	c.m.Unlock()
}

func TestRecursiveLock(t *testing.T) {
	c := NewCalc()

	wg := sync.WaitGroup{}
	wg.Add(1)

	go c.Do(&wg)

	wg.Wait()
}
