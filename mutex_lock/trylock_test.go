package mutex_lock

import (
	"github.com/modern-go/gls"
	"log"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

type RecursiveMutex2 struct {
	internalMutex    sync.Mutex // 不同协程竞争这一把锁
	currentGoRoutine int64      // 当前持有锁的协程 id
	lockCount        uint64     // 当前锁重复加锁次数
}

func (rm *RecursiveMutex2) Lock() {
	// 获取当前协程id
	goRoutineID := gls.GoID()

	// 自旋等待，直到TryLock=true
	for {
		bLock := rm.internalMutex.TryLock()

		if bLock {
			// 锁当前没被任何协程持有，加锁成功
			atomic.StoreInt64(&rm.currentGoRoutine, goRoutineID)
			break
		} else {
			if atomic.CompareAndSwapInt64(&rm.currentGoRoutine, goRoutineID, goRoutineID) {
				// 当前协程重复加锁，直接退出，加锁成功
				break
			} else {
				// 不同的协程获取锁，等待其它持有锁的协程释放(TryLock=true)
				time.Sleep(time.Millisecond)
				continue
			}
		}
	}

	// 增加当前锁重入次数
	rm.lockCount++
}

func (rm *RecursiveMutex2) Unlock() {
	// 减少锁重入次数，=0时释放锁
	rm.lockCount--
	if rm.lockCount == 0 {
		atomic.StoreInt64(&rm.currentGoRoutine, 0)
		rm.internalMutex.Unlock()
	}
}

type Calc3 struct {
	m RecursiveMutex2
}

func NewCalc3() *Calc3 {
	return &Calc3{RecursiveMutex2{sync.Mutex{}, 0, 0}}
}

func (c *Calc3) Do(wg *sync.WaitGroup) {
	defer wg.Done()

	c.m.Lock()

	log.Println("Do start")
	c.detailDo()
	log.Println("Do end")

	c.m.Unlock()
}

func (c *Calc3) detailDo() {
	c.m.Lock()

	log.Println("detailDo start")

	c.m.Unlock()
}

func TestRecursiveLock3(t *testing.T) {
	c := NewCalc3()

	wg := sync.WaitGroup{}
	wg.Add(2)

	go c.Do(&wg)
	go c.Do(&wg)

	wg.Wait()
}
