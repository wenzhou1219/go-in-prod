package mutex_lock

import (
	"github.com/modern-go/gls"
	"log"
	"sync"
	"testing"
	"time"
)

type RecursiveMutex struct {
	internalMutex    sync.Mutex // 保护 currentGoRoutine + lockCount
	currentGoRoutine int64      // 当前持有锁的协程 id
	lockCount        uint64     // 当前锁重复加锁次数
}

func (rm *RecursiveMutex) Lock() {
	// 获取当前协程id
	goRoutineID := gls.GoID()

	// 自旋锁实现mutex，效率相对较低
	// 一直自旋等到 currentGoRoutine=0 则成功获取锁
	for {
		rm.internalMutex.Lock()

		if rm.currentGoRoutine == 0 {
			// 锁当前没被任何协程持有，加锁成功
			rm.currentGoRoutine = goRoutineID
			break
		} else if rm.currentGoRoutine == goRoutineID {
			// 当前协程重复加锁，直接退出，加锁成功
			break
		} else {
			// 不同的协程获取锁，等待其它持有锁的协程释放(currentGoRoutine=0)
			rm.internalMutex.Unlock()

			time.Sleep(time.Millisecond)
			continue
		}
	}

	// 增加当前锁重入次数
	rm.lockCount++
	rm.internalMutex.Unlock()
}

func (rm *RecursiveMutex) Unlock() {
	rm.internalMutex.Lock()
	defer rm.internalMutex.Unlock()

	// 减少锁重入次数，=0时释放锁
	rm.lockCount--
	if rm.lockCount == 0 {
		rm.currentGoRoutine = 0
	}
}

type Calc2 struct {
	m RecursiveMutex
}

func NewCalc2() *Calc2 {
	return &Calc2{RecursiveMutex{sync.Mutex{}, 0, 0}}
}

func (c *Calc2) Do(wg *sync.WaitGroup) {
	defer wg.Done()

	c.m.Lock()

	log.Println("Do start")
	c.detailDo()
	log.Println("Do end")

	c.m.Unlock()
}

func (c *Calc2) detailDo() {
	c.m.Lock()

	log.Println("detailDo start")

	c.m.Unlock()
}

func TestRecursiveLock2(t *testing.T) {
	c := NewCalc2()

	wg := sync.WaitGroup{}
	wg.Add(2)

	go c.Do(&wg)
	go c.Do(&wg)

	wg.Wait()
}
