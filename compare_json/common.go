package compare_json

import "io/ioutil"

var (
	SMALL_LOG_DATA []byte
)

func init() {
	SMALL_LOG_DATA, _ = ioutil.ReadFile("data/log-small.json")
}
