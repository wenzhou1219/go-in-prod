package compare_json

import (
	"fmt"
	"github.com/ohler55/ojg/jp"
	"github.com/ohler55/ojg/oj"
	"testing"
)

func TestOjgJson(t *testing.T) {
	x, err := jp.ParseString("st")
	if err != nil {
		fmt.Print(err)
		return
	}

	obj, err := oj.Parse(SMALL_LOG_DATA)
	if err != nil {
		fmt.Print(err)
		return
	}

	v := x.Get(obj)
	if v[0] == nil {
		fmt.Println("nil value")
	}

	fmt.Printf("int value st:%v\n", v)
}
