package compare_json

import (
	"fmt"
	json2 "github.com/goccy/go-json"
	"testing"
)

func TestGoJson(t *testing.T) {
	var v map[string]interface{}

	err := json2.Unmarshal([]byte(`{
	                "str": "bar",
	                "int": 123,
	                "float": 1.23,
	                "bool": true,
	                "arr": [1, "foo", {}]
	        }`), &v)
	if err != nil {
		t.Fatal("err parse")
	}

	fmt.Printf("foo=%s\n", v["str"])
	fmt.Printf("int=%v\n", v["int"])
	fmt.Printf("float=%f\n", v["float"])
	fmt.Printf("bool=%v\n", v["bool"])
	fmt.Printf("arr.1=%v\n", v["arr"])
}
