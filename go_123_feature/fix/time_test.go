package fix

import (
	"testing"
	"time"
)

// https://github.com/golang/go/issues/14383
// https://mp.weixin.qq.com/s/NijdOmdfKGLJowhbe9yqPg
// https://mp.weixin.qq.com/s/Qcpj7TqMeOwCs--59kD3Kw
// 注意go.mod >=1.23

func TestTimerReset(t *testing.T) {
	timeout := 50 * time.Millisecond
	tt := time.NewTimer(timeout)

	time.Sleep(100 * time.Millisecond)

	start := time.Now()
	tt.Reset(timeout)
	<-tt.C

	t.Log(time.Since(start).Milliseconds())
}

func TestTimerReset2(t *testing.T) {
	timeout := 50 * time.Millisecond
	tt := time.NewTimer(timeout)

	time.Sleep(100 * time.Millisecond)

	<-tt.C
	start := time.Now()
	tt.Reset(timeout)
	<-tt.C

	t.Log(time.Since(start).Milliseconds())
}

func TestTimerAfter(t *testing.T) {
	ch := make(chan int, 10)
	go func() {
		in := 1
		for {
			ch <- in
			in++
		}
	}()

	for {
		select {
		case _ = <-ch:
			continue
		case <-time.After(10 * time.Second):
			t.Log("time", time.Now().Unix())
		}
	}
}

func TestTimerAfter2(t *testing.T) {
	ch := make(chan int, 10)
	go func() {
		in := 1
		for {
			ch <- in
			in++
		}
	}()

	tt := time.After(10 * time.Second)

	for {
		select {
		case _ = <-ch:
			continue
		case <-tt:
			t.Log("time", time.Now().Unix())
		}
	}
}

func TestTimerAfter3(t *testing.T) {
	ch := make(chan int, 10)
	go func() {
		in := 1
		for {
			ch <- in
			in++
		}
	}()

	tt := time.NewTimer(10 * time.Second)
	defer tt.Stop()

	for {
		select {
		case _ = <-ch:
			continue
		case <-tt.C:
			t.Log("time", time.Now().Unix())
		}
	}
}
