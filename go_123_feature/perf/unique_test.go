package perf

import (
	"testing"
	"unique"
)

// https://pkg.go.dev/go4.org/intern#section-readme
// https://github.com/golang/go/issues/62483
// https://mp.weixin.qq.com/s/NDqeknAm7q77siHm0Jbcxg

func TestGoUniqueString(t *testing.T) {
	s1 := unique.Make("hello1")
	s2 := unique.Make("hello1")
	s3 := unique.Make("hello3")

	// 输出三个变量指针地址
	t.Log(&s1, &s2, &s3)

	if s1 != s2 {
		t.Fatal(s1, s2)
	}

	if s1 == s3 {
		t.Fatal(s1, s3)
	}
}
