package iter

import (
	"iter"
	"testing"
)

// https://github.com/golang/go/issues/61405
// https://tonybai.com/2024/06/24/range-over-func-and-package-iter-in-go-1-23/
// https://mp.weixin.qq.com/s/In_CxhOikw3iZYBnEXgG_g

//第一类：function, 0 values, f的签名为func(func() bool)
//for range f { ... }
//
//第二类：function, 1 value，f的签名为func(func(V) bool)
//for x := range f { ... }
//
//第三类：function, 2 values，f的签名为func(func(K, V) bool)
//
//for x, y := range f { ... }
//for x, _ := range f { ... }
//for _, y := range f { ... }

// 迭代 （最初有，暂时取消，自己实现）
type Seq0 func(yield func() bool)

func iter0[Slice ~[]E, E any](s Slice) Seq0 {
	return func(yield func() bool) {
		for range s {
			if !yield() {
				return
			}
		}
	}
}

func TestIter0(t *testing.T) {
	count := 0
	s := []int{0, 1, 2, 3, 4, 5, 6}

	for range iter0(s) {
		count++
	}

	if count != 7 {
		t.Fatal(count)
	}
	t.Log("total count", count)
}

// 单值迭代
func IntRange(v int) iter.Seq[int] {
	return func(yield func(int) bool) {
		for i := 0; i < v; i++ {
			if !yield(i) {
				return
			}
		}
	}
}

func TestIntRange(t *testing.T) {
	count := 0
	for i := range IntRange(10) {
		t.Log(i)
		count++
	}

	if count != 10 {
		t.Fatal(count)
	}
	t.Log("total count", count)
}

// 多值迭代
func StringBackwards(s []string) iter.Seq2[int, string] {
	return func(yield func(int, string) bool) {
		for i := len(s) - 1; i >= 0; i-- {
			if !yield(i, s[i]) {
				return
			}
		}
	}
}

func TestStringBackwards(t *testing.T) {
	s := []string{"a", "b", "c", "d", "e", "f"}

	count := 0
	for i, v := range StringBackwards(s) {
		t.Log(i, v)
		count++
	}

	if count != 6 {
		t.Fatal(count)
	}
	t.Log("total count", count)
}
