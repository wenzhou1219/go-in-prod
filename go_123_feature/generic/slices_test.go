package generic

import (
	"slices"
	"testing"
)

// 1.22 concat
func TestConTact(t *testing.T) {
	s1 := []string{"a", "b", "c"}
	s2 := []string{"d", "e", "f"}
	s3 := []string{"g", "h", "i"}

	s := slices.Concat(s1, s2, s3)
	t.Log(s)
}

// 1.23 repeat
func TestRepeat(t *testing.T) {
	s1 := []string{"a", "b"}
	s := slices.Repeat(s1, 3)

	t.Log(s)
}
