package generic

import (
	"maps"
	"slices"
	"testing"
)

// slices iter方法
func TestSlicesIter(t *testing.T) {
	s := []string{"a", "b", "c", "d", "e", "f"}

	for k, v := range slices.All(s) {
		t.Log(k, v)
	}

	for v := range slices.Values(s) {
		t.Log(v)
	}

	for k, v := range slices.Backward(s) {
		t.Log(k, v)
	}
}

// maps iter方法
// 注意无序
func TestMapsIter(t *testing.T) {
	m := map[string]int{"a": 1, "b": 2, "c": 3, "d": 4, "e": 5, "f": 6}

	for k, v := range maps.All(m) {
		t.Log(k, v)
	}

	for k := range maps.Keys(m) {
		t.Log(k)
	}

	for v := range maps.Values(m) {
		t.Log(v)
	}
}
