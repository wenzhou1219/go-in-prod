package generic

import "testing"

// https://mp.weixin.qq.com/s/gjBXmNZlacEpddjgy2umhg
// go <= 1.23; GOEXPERIMENT=aliastypeparams;GODEBUG=gotypesalias=1

type MySlice[T any] = []T

func TestMySliceType(t *testing.T) {
	s := MySlice[string]{"test1", "test2", "test2"}

	for i, v := range s {
		t.Log(i, v)
	}
}
