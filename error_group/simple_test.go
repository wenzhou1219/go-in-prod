package error_group

import (
	"context"
	"errors"
	"fmt"
	"github.com/hashicorp/go-multierror"
	"golang.org/x/sync/errgroup"
	"log"
	"sync"
	"testing"
)

type MyError struct {
}

func (e MyError) Error() string {
	return "Something wrong 2!"
}

var (
	Error1 = errors.New("Something wrong 1!")
	Error2 = MyError{}
)

func doSomething() error {
	return Error1
}

func doSomething2(data interface{}) error {
	return Error2
}

// 出错中断协程，打印日志，退出

func TestSimpleExit(t *testing.T) {
	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()

		//do something
		if err := doSomething(); err != nil {
			t.Logf("Error when call doSomething:%v\n", err)
			return
		}
	}()

	wg.Wait()
}

// 单独通道，监控错误，打印日志或退出(自己控制退出策略)
func TestSimpleChannel(t *testing.T) {
	wg := sync.WaitGroup{}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	dataChan := make(chan int)
	errChan := make(chan error)

	defer close(dataChan)
	defer close(errChan)

	// 数据处理流程
	dataFunc := func(ctx context.Context, dataChan chan int, errChan chan error) {
		defer wg.Done()

		for {
			select {
			case v, ok := <-dataChan:
				if !ok {
					log.Println("Receive data channel close msg!")
					return
				}

				if err := doSomething2(v); err != nil {
					errChan <- err
					continue
				}

				// do ...

			case <-ctx.Done():
				log.Println("Receive exit msg!")
				return
			}
		}
	}

	wg.Add(1)
	go dataFunc(ctx, dataChan, errChan)

	wg.Add(1)
	go dataFunc(ctx, dataChan, errChan)

	// 错误处理流程，error处理通道异步等待
	wg.Add(1)
	go func(errChan chan error) {
		defer wg.Done()

		for {
			select {
			case v, ok := <-errChan:
				if !ok {
					log.Println("Receice err channel close msg!")
					return
				}

				// 收到错误时，可选择记录日志或退出
				if v != nil {
					t.Logf("Error when call doSomething:%v\n", v)
					cancel() // 通知全部退出
					return
				}

			case <-ctx.Done():
				log.Println("Receive exit msg!")
				return
			}
		}

	}(errChan)

	dataChan <- 1
	wg.Wait()
}

func TestSimpleChannel2(t *testing.T) {
	wg := sync.WaitGroup{}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	dataChan := make(chan int)
	errChan := make(chan error)

	defer close(dataChan)
	defer close(errChan)

	// 数据处理流程
	dataFunc := func(ctx context.Context, dataChan chan int, errChan chan error) {
		defer wg.Done()

		for {
			select {
			case v, ok := <-dataChan:
				if !ok {
					log.Println("Receive data channel close msg!")
					return
				}

				if err := doSomething2(v); err != nil {
					errChan <- err
					continue
				}

				// do ...

			case <-ctx.Done():
				log.Println("Receive exit msg!")
				return
			}
		}
	}

	wg.Add(1)
	go dataFunc(ctx, dataChan, errChan)

	wg.Add(1)
	go dataFunc(ctx, dataChan, errChan)

	dataChan <- 1

	// 错误处理流程，error处理通道同步等待
	for {
		select {
		case v, ok := <-errChan:
			if !ok {
				log.Println("Receice err channel close msg!")
				goto EXIT
			}

			// 收到错误时，可选择记录日志或退出
			if v != nil {
				t.Logf("Error when call doSomething:%v\n", v)
				cancel()
				goto EXIT
			}

		case <-ctx.Done():
			log.Println("Receive exit msg!")
			goto EXIT
		}
	}

EXIT:
	wg.Wait()
}

// 多路协程error合并，用于多路check场景
func TestSimpleChannel3(t *testing.T) {

	// 同步执行多个任务，返回error合并
	var err = func() error {
		var result error

		if err := doSomething(); err != nil {
			result = multierror.Append(result, err)
		}

		if err := doSomething2(nil); err != nil {
			result = multierror.Append(result, err)
		}

		return result
	}()

	// 打印输出
	if err != nil {
		fmt.Printf("%v\n", err)
	}

	// 获取错误列表
	if err != nil {
		if merr, ok := err.(*multierror.Error); ok {
			fmt.Printf("%v\n", merr.Errors)
		}
	}

	// 判断是否为某种类型
	if err != nil && errors.Is(err, Error1) {
		fmt.Println("Errors contain error 1")
	}

	// 判断是否其中一个error能够转换成指定error
	var e MyError
	if err != nil && errors.As(err, &e) {
		fmt.Println("One Error can be convert to nyerror")
	}
}

func TestSimpleChannel4(t *testing.T) {
	wg := sync.WaitGroup{}

	taskNum := 10
	errChan := make(chan error, taskNum)

	// 异步执行多个任务
	step := func(stepNum int, errChan chan error) {
		defer wg.Done()
		errChan <- fmt.Errorf("step %d error", stepNum)
	}

	for i := 0; i < taskNum; i++ {
		wg.Add(1)
		go step(i, errChan)
	}

	// 等待任务完成
	go func() {
		wg.Wait()
		close(errChan)
	}()

	// err通道阻塞等待，可能的所有错误合并
	var result *multierror.Error
	for err := range errChan {
		result = multierror.Append(result, err)
	}

	// 出现一个错误时，选择记录日志或退出
	if len(result.Errors) != 0 {
		log.Println(result.Errors)
	}
}

// 借助error group
func TestSimpleChannel5(t *testing.T) {
	eg, ctx := errgroup.WithContext(context.Background())

	dataChan := make(chan int)
	defer close(dataChan)

	// 数据处理流程
	dataFunc := func() error {
		for {
			select {
			case v, ok := <-dataChan:
				if !ok {
					log.Println("Receive data channel close msg!")
					return nil
				}

				if err := doSomething2(v); err != nil {
					return err
				}

				// do ...

			// 增加ctx通知完成
			case <-ctx.Done():
				log.Println("Receive exit msg!")
				return nil
			}
		}
	}

	eg.Go(dataFunc)
	eg.Go(dataFunc)
	eg.Go(dataFunc)

	dataChan <- 1

	// 错误处理流程，任何一个协程出现error，则会调用ctx对应cancel函数，所有相关协程都会退出
	if err := eg.Wait(); err != nil {
		fmt.Printf("Something is wrong->%v\n", err)
	}
}
