package compare_jsonpath

import (
	"github.com/JimWen/fastjson"
	"github.com/bhmj/jsonslice"
	"github.com/ohler55/ojg/jp"
	"github.com/ohler55/ojg/oj"
	"github.com/spyzhov/ajson"
	"testing"
)

func MyTestData(t *testing.T, data []byte, expression string) {
	v, _ := jsonslice.Get(data, expression)
	t.Logf("%v", string(v))

	v2, _ := ajson.JSONPath(data, expression)
	t.Logf("%v", v2)

	x, err := jp.ParseString(expression)
	if err != nil {
		return
	}

	obj, err := oj.Parse(data)
	if err != nil {
		return
	}
	t.Logf("%v", x.Get(obj))
}

func TestMedium(t *testing.T) {
	MyTestData(t, MEDIUM_LOG_DATA, "$.person.twitter.handle")
}

func TestLarge(t *testing.T) {
	MyTestData(t, LARGE_LOG_DATA, "$.topics.topics..last_posted_at")
}

func BenchmarkJsonsliceMedium(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_, err := jsonslice.Get(MEDIUM_LOG_DATA, "$.person.twitter.handle")
		if err != nil {
			continue
		}
	}
}

func BenchmarkOjgMedium(b *testing.B) {
	b.ReportAllocs()
	x, err := jp.ParseString("$.person.twitter.handle")
	if err != nil {
		return
	}

	obj, err := oj.Parse(MEDIUM_LOG_DATA)
	if err != nil {
		return
	}

	for i := 0; i < b.N; i++ {
		_ = x.Get(obj)
	}
}

func BenchmarkAJsonMedium(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_, err := ajson.JSONPath(MEDIUM_LOG_DATA, "$.person.twitter.handle")
		if err != nil {
			continue
		}
	}
}

func BenchmarkFastjsonMedium(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		obj, err := fastjson.ParseBytes(MEDIUM_LOG_DATA)
		if err != nil {
			return
		}

		_, err = fastjson.JsonPathLookup(obj, "$.person.twitter.handle")
		if err != nil {
			continue
		}
	}
}

func BenchmarkFastjsonMedium2(b *testing.B) {
	c, err := fastjson.Compile("$.person.twitter.handle")
	if err != nil {
		return
	}

	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		obj, err := fastjson.ParseBytes(MEDIUM_LOG_DATA)
		if err != nil {
			return
		}

		_, err = c.Lookup(obj)
		if err != nil {
			continue
		}
	}
}

func BenchmarkJsonsliceLarge(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_, err := jsonslice.Get(LARGE_LOG_DATA, "$.topics.topics..last_posted_at")
		if err != nil {
			continue
		}
	}
}

func BenchmarkOjgLarge(b *testing.B) {
	b.ReportAllocs()
	x, err := jp.ParseString("$.topics.topics..last_posted_at")
	if err != nil {
		return
	}

	obj, err := oj.Parse(LARGE_LOG_DATA)
	if err != nil {
		return
	}

	for i := 0; i < b.N; i++ {
		_ = x.Get(obj)
	}
}

func BenchmarkAJsonLarge(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_, err := ajson.JSONPath(LARGE_LOG_DATA, "$.topics.topics..last_posted_at")
		if err != nil {
			continue
		}
	}
}

func BenchmarkFastjsonLarge(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		obj, err := fastjson.ParseBytes(LARGE_LOG_DATA)
		if err != nil {
			return
		}

		_, err = fastjson.JsonPathLookup(obj, "$.person.twitter.handle")
		if err != nil {
			continue
		}
	}
}

func BenchmarkFastjsonLarge2(b *testing.B) {
	c, err := fastjson.Compile("$.person.twitter.handle")
	if err != nil {
		return
	}

	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		obj, err := fastjson.ParseBytes(LARGE_LOG_DATA)
		if err != nil {
			return
		}

		_, err = c.Lookup(obj)
		if err != nil {
			continue
		}
	}
}
