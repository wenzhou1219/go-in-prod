package compare_jsonpath

import "io/ioutil"

type JsonPathNode struct {
	expression  string
	description string
	ch          string
}

var (
	// https://goessner.net/articles/JsonPath/
	bookFile  = "data/book.json"
	BOOK_DATA []byte

	// https://github.com/json-iterator/go-benchmark/blob/master/src/github.com/json-iterator/go-benchmark/benchmark.go
	smallLogFile  = "data/log-small.json"
	mediumLogFile = "data/log-medium.json"
	largeLogFile  = "data/log-large.json"

	SMALL_LOG_DATA  []byte
	MEDIUM_LOG_DATA []byte
	LARGE_LOG_DATA  []byte

	// https://github.com/ohler55/compare-go-json/tree/master/data
	patientFile  = "data/patient.json"
	PATIENT_DATA []byte
)

// BOOK_QUERY
// 参考
// https://goessner.net/articles/JsonPath/
// https://www.jianshu.com/p/634bd5d9d944
var BOOK_QUERY = []JsonPathNode{
	{"$.store.book[*].author", "the authors of all books in the store", "获取书店所有书籍作者"},
	{"$..author", "all authors", "获取所有书籍作者名称"},
	{"$.store.*", "all things in store, which are some books and a red bicycle.", "获取书店所有信息"},
	{"$.store..price", "the price of everything in the store.", "获取书店所有商品价格信息"},
	{"$..book[2]", "the third book", "获取书店第三本书信息"},
	{"$..book[(@.length-1)]", "the last book in order.", "获取书店最后一本书信息"},
	{"$..book[-1:]", "the last book in order.", "获取书店最后一本书信息"},
	{"$..book[0,1]", "the first two books", "获取书店第一、二本书信息"},
	{"$..book[:2]", "the first two books", "获取书店第一、二本书信息"},
	{"$..book[?(@.isbn)]", "filter all books with isbn number", "过滤操作（获取含有isbn属性的书籍信息）"},
	{"$..book[?(@.price<10)]", "filter all books cheapier than 10", "过滤操作（获取price小于10元的书籍信息）"},
	{"$..*", "all Elements in XML document. All members of JSON structure.", "获取所有信息"},

	{"$[?(@.expensive == 10)]", "filter JSON object structure.", "按对象过滤"},
}

func init() {
	BOOK_DATA, _ = ioutil.ReadFile(bookFile)

	SMALL_LOG_DATA, _ = ioutil.ReadFile(smallLogFile)
	MEDIUM_LOG_DATA, _ = ioutil.ReadFile(mediumLogFile)
	LARGE_LOG_DATA, _ = ioutil.ReadFile(largeLogFile)

	PATIENT_DATA, _ = ioutil.ReadFile(patientFile)
}
