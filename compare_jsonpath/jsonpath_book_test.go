package compare_jsonpath

import (
	"github.com/bhmj/jsonslice"
	"github.com/ohler55/ojg/jp"
	"github.com/ohler55/ojg/oj"
	"github.com/spyzhov/ajson"
	"log"
	"testing"
)

func TestJsonsliceBookFunction(t *testing.T) {
	var data = BOOK_DATA
	var unsupport []JsonPathNode

	for _, node := range BOOK_QUERY {
		v, err := jsonslice.Get(data, node.expression)
		if err != nil || v == nil || len(v) == 0 {
			unsupport = append(unsupport, node)
			continue
		}

		t.Logf("Value:%s, Des:%s", string(v), node.ch)
	}

	t.Logf("不支持的JsonPath表达式")
	for _, node := range unsupport {
		t.Logf("Express:%s, Des:%s", node.expression, node.ch)
	}
}

func TestOjgBookFunction(t *testing.T) {
	var data = BOOK_DATA
	var unsupport []JsonPathNode

	for _, node := range BOOK_QUERY {
		x, err := jp.ParseString(node.expression)
		if err != nil {
			unsupport = append(unsupport, node)
			continue
		}

		obj, err := oj.Parse(data[:])
		if err != nil {
			log.Fatalln("解析json错误")
			continue
		}

		v := x.Get(obj)
		if v == nil || len(v) == 0 {
			unsupport = append(unsupport, node)
			continue
		}

		t.Logf("Value:%v, Des:%s", oj.JSON(v), node.ch)
	}

	t.Logf("不支持的JsonPath表达式")
	for _, node := range unsupport {
		t.Logf("Express:%s, Des:%s", node.expression, node.ch)
	}
}

func TestAJsonBookFunction(t *testing.T) {
	var data = BOOK_DATA
	var unsupport []JsonPathNode

	for _, node := range BOOK_QUERY {
		v, err := ajson.JSONPath(data, node.expression)
		if err != nil || v == nil || len(v) == 0 {
			unsupport = append(unsupport, node)
			continue
		}

		t.Logf("Value:%v, Des:%s", v, node.ch)
	}

	t.Logf("不支持的JsonPath表达式")
	for _, node := range unsupport {
		t.Logf("Express:%s, Des:%s", node.expression, node.ch)
	}
}
