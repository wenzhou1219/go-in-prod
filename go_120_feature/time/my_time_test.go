package time

import (
	"fmt"
	"testing"
	"time"
)

func TestTimeFormat(t *testing.T) {
	tm1 := time.Now()

	fmt.Printf("DateTime-%v\nDateOnly-%v\nTimeOnly-%v\n",
		tm1.Format(time.DateTime),
		tm1.Format(time.DateOnly),
		tm1.Format(time.TimeOnly))
}

func TestTimeCompare(t *testing.T) {
	tm1 := time.Now()
	tm2 := time.Now()

	c := tm1.Compare(tm2)
	if c == -1 {
		fmt.Println("tm1 < tm2")
	} else if c == 0 {
		fmt.Println("tm1 = tm2")
	} else if c == 1 {
		fmt.Println("tm1 > tm2")
	}
}
