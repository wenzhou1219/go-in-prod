package main

import "fmt"

func main() {
	slice := []int{1, 2, 3, 4, 5}

	// 老方法
	array1 := *(*[5]int)(slice)

	fmt.Printf("Slice:%v\n", slice)
	fmt.Printf("Array:%v\n", array1)

	// 	panic: runtime error: cannot convert slice with length 5 to array or pointer to array with length 6
	array11 := *(*[6]int)(slice)
	fmt.Printf("Array:%v\n", array11)

	//新方法
	// 1.20之前报错 cannot convert slice (variable of type []int) to type [5]int
	array2 := [5]int(slice)

	fmt.Printf("Slice:%v\n", slice)
	fmt.Printf("Array:%v\n", array2)

	// panic: runtime error: cannot convert slice with length 5 to array or pointer to array with length 6
	//array22 := [6]int(slice)
	//fmt.Printf("Array:%v\n", array22)
}
