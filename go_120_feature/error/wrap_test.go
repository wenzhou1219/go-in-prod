package error

import (
	"errors"
	"fmt"
	"testing"
)

func TestMultiWrap(t *testing.T) {
	err1 := errors.New("Error 1")
	err2 := errors.New("Error 2")
	err3 := errors.New("Error 3")

	err := fmt.Errorf("%w,%w,%w", err1, err2, err3)

	fmt.Printf("wrap errors:%v\n", err)

	fmt.Printf("is err1:%v, is err2:%v, is err3:%v\n",
		errors.Is(err, err1),
		errors.Is(err, err2),
		errors.Is(err, err3))
}
