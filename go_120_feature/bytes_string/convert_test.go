package bytes_string

import (
	"fmt"
	"reflect"
	"testing"
	"unsafe"
)

func OldBytesToString(b []byte) string {
	return *((*string)(unsafe.Pointer(&b)))
}

func OldStringToBytes(s string) []byte {
	stringHeader := (*reflect.StringHeader)(unsafe.Pointer(&s))

	var b []byte
	pbytes := (*reflect.SliceHeader)(unsafe.Pointer(&b)) // 先引用，防止原有string gc
	pbytes.Data = stringHeader.Data
	pbytes.Len = stringHeader.Len
	pbytes.Cap = stringHeader.Len

	return b
}

func NewBytesToString(b []byte) string {
	return unsafe.String(&b[0], len(b))
}

func NewStringToBytes(s string) []byte {
	return unsafe.Slice(unsafe.StringData(s), len(s))
}

func TestOldConvert(t *testing.T) {
	s := "hello"
	b := []byte("word")

	fmt.Printf("string to byte:%v\n", OldStringToBytes(s))
	fmt.Printf("byte to string:%v\n", OldBytesToString(b))
}

func TestNewConvert(t *testing.T) {
	s := "hello"
	b := []byte("word")

	fmt.Printf("string to byte:%v\n", NewStringToBytes(s))
	fmt.Printf("byte to string:%v\n", NewBytesToString(b))
}
