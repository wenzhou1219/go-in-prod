//go:build goexperiment.arenas

package myarena

import (
	"arena"
	"sync"
	"testing"
)

type MyObj struct {
	Index int
}

func BenchmarkCreateObj(b *testing.B) {
	b.ReportAllocs()

	var p *MyObj

	for i := 0; i < b.N; i++ {
		for j := 0; j < 1000; j++ {
			p = new(MyObj)
			p.Index = j
		}
	}
}

var (
	objPool = sync.Pool{
		New: func() interface{} {
			return &MyObj{}
		},
	}
)

func BenchmarkCreateObj_SyncPool(b *testing.B) {
	b.ReportAllocs()

	var p *MyObj

	for i := 0; i < b.N; i++ {
		for j := 0; j < 1000; j++ {
			p = objPool.Get().(*MyObj)
			p.Index = 23
			objPool.Put(p)
		}
	}
}

func BenchmarkCreateObj_Arena(b *testing.B) {
	b.ReportAllocs()

	var p *MyObj

	a := arena.NewArena()
	defer a.Free()

	for i := 0; i < b.N; i++ {

		for j := 0; j < 1000; j++ {
			p = arena.New[MyObj](a)
			p.Index = 23
		}

	}
}
