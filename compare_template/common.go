package compare_template

import (
	"html/template"
	"strings"
)

type UserInfo struct {
	Name       string
	Gender     string
	Age        int
	IsYoung    bool
	Likes      []string
	Parents    map[string]string
	UnsafeCode string
}

func addName(arg string) (string, error) {
	return arg + "美美哒", nil
}

func getSafeString(s string) template.HTML {
	s2 := strings.ReplaceAll(s, "script", "noscript")
	return template.HTML(s2)
}
