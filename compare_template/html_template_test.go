package compare_template

import (
	"fmt"
	"html/template"
	"net/http"
	"testing"
)

// 参考 https://zhuanlan.zhihu.com/p/299048675
// https://studygolang.com/pkgdoc
// https://blog.csdn.net/xingzuo_1840/article/details/126590348
func sayHello(w http.ResponseWriter, r *http.Request) {
	// 解析指定文件生成模板对象
	tmpl, err := template.
		New("hello.tmpl").
		Funcs(template.FuncMap{
			"add":  addName,
			"safe": getSafeString,
		}).
		Delims("{{", "}}").
		ParseFiles("./data/hello.tmpl")

	if err != nil {
		fmt.Println("create template failed, err:", err)
		return
	}

	// 利用给定数据渲染模板, 并将结果写入w
	user := UserInfo{
		Name:       "小明",
		Gender:     "男",
		Age:        18,
		IsYoung:    false,
		Likes:      []string{"电影", "读书", "跑步"},
		Parents:    map[string]string{"father": "xxx", "mother": "yyy"},
		UnsafeCode: "<script>alert('testtest')</script>",
	}

	if user.Age >= 18 {
		user.IsYoung = true
	}

	err = tmpl.Execute(w, user)
	if err != nil {
		fmt.Println("Execute template failed, err:", err)
		return
	}
}

func showPage(w http.ResponseWriter, r *http.Request) {
	// 解析指定文件生成模板对象
	tmpl, err := template.
		ParseGlob("./data2/*.tmpl")
	if err != nil {
		fmt.Println("create template failed, err:", err)
		return
	}

	content := "网页正文"

	err = tmpl.Execute(w, content)
	if err != nil {
		fmt.Println("Execute template failed, err:", err)
		return
	}
}

func Test_html_template(t *testing.T) {
	http.HandleFunc("/", sayHello)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Println("HTTP SERVER failed,err:", err)
		return
	}
}

func Test_html_template2(t *testing.T) {
	http.HandleFunc("/", showPage)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		fmt.Println("HTTP SERVER failed,err:", err)
		return
	}
}
