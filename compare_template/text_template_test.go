package compare_template

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"testing"
	"text/template"
)

// 参考 https://zhuanlan.zhihu.com/p/299048675
func sayTextHello(wr io.Writer) {
	// 解析指定文件生成模板对象
	tmpl, err := template.
		New("hello.tmpl"). // 必须和file base name一致
		Funcs(template.FuncMap{
			"add":  addName,
			"safe": getSafeString,
		}).
		Delims("{{", "}}").
		ParseFiles("./data/hello.tmpl")

	if err != nil {
		fmt.Println("create template failed, err:", err)
		return
	}

	fmt.Println(filepath.Base("./data/hello.tmpl"))

	// 利用给定数据渲染模板, 并将结果写入w
	user := UserInfo{
		Name:       "小明",
		Gender:     "男",
		Age:        18,
		IsYoung:    false,
		Likes:      []string{"电影", "读书", "跑步"},
		Parents:    map[string]string{"father": "xxx", "mother": "yyy"},
		UnsafeCode: "<script>alert('testtest')</script>",
	}

	if user.Age >= 18 {
		user.IsYoung = true
	}

	err = tmpl.Execute(wr, user)
	if err != nil {
		fmt.Println("Execute template failed, err:", err)
		return
	}
}

func Test_text_template(t *testing.T) {
	sayTextHello(os.Stdout)
}
