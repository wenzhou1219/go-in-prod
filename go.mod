module go-in-prod

go 1.24

require (
	github.com/BurntSushi/toml v1.2.1
	github.com/JimWen/configuration v1.0.1
	github.com/JimWen/fastjson v1.0.6
	github.com/bhmj/jsonslice v1.1.2
	github.com/goccy/go-json v0.10.2
	github.com/gurkankaymak/hocon v1.2.16
	github.com/hashicorp/go-multierror v1.1.1
	github.com/jessevdk/go-flags v1.5.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/modern-go/gls v0.0.0-20220109145502-612d0167dce5
	github.com/ohler55/ojg v1.17.3
	github.com/panjf2000/ants/v2 v2.7.1
	github.com/pkg/errors v0.9.1
	github.com/samber/lo v1.37.0
	github.com/spf13/cobra v1.6.1
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.15.0
	github.com/spyzhov/ajson v0.7.2
	github.com/urfave/cli v1.22.12
	golang.org/x/sync v0.1.0
	golang.org/x/time v0.3.0
	gopkg.in/ini.v1 v1.67.0
)

require (
	github.com/bhmj/xpression v0.9.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/magiconair/properties v1.8.7 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pelletier/go-toml/v2 v2.0.6 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/spf13/afero v1.9.3 // indirect
	github.com/spf13/cast v1.5.0 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/subosito/gotenv v1.4.2 // indirect
	golang.org/x/exp v0.0.0-20220303212507-bbda1eaf7a17 // indirect
	golang.org/x/sys v0.3.0 // indirect
	golang.org/x/text v0.5.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
