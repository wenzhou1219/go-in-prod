package compare_config

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"log"
	"testing"
	"time"
)

// TomlConfig
// 配置格式参考 https://toml.io/en/v1.0.0#filename-extension
type TomlConfig struct {
	Age        int
	Cats       []string
	Pi         float64
	Perfection []int
	DOB        time.Time
	LogExpire  time.Duration `toml:"logExpire"`
	Port       int           `toml:"port"`

	Section_config Section_config `toml:"section_config"`
	List_config    List_config    `toml:"list_config"`
	Map_config     []Map_config   `toml:"map_config"`
}

type Section_config struct {
	User     string `toml:"user"`
	PassWord string `toml:"passWord"`
}

type List_config struct {
	Processes [][]string `toml:"processes"`
}

type Map_config struct {
	Name string `toml:"Name"`
	Sku  int64  `toml:"sku"`
}

func TestToml(t *testing.T) {
	var conf TomlConfig

	if _, err := toml.DecodeFile("data/my.toml", &conf); err != nil {
		log.Fatalf("parse config file error %v", err)
	}

	fmt.Println(conf)

}
