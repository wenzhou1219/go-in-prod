package compare_config

import (
	"fmt"
	"github.com/gurkankaymak/hocon"
	"os"
	"testing"
)

// HOCON
// 格式参考 https://github.com/lightbend/config/blob/main/HOCON.md
func TestHconf(t *testing.T) {
	conf, err := hocon.ParseResource("data/my.conf")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	intValue := conf.GetInt("app.intVal")
	floatValue := conf.GetFloat64("app.floatVal")
	stringValue := conf.GetString("app.stringValue")
	durationVal := conf.GetDuration("app.durationVal")

	fmt.Printf("NormalData: %v %v %v %v\n", intValue, floatValue, stringValue, durationVal)

	arrayValue := conf.GetArray("app.arrays.ofInt")
	objectValue := conf.GetObject("app.objects")

	fmt.Printf("ArrayData: %v ObjectData: %v\n", arrayValue, objectValue)

	multiArrayValue := conf.GetArray("multi_arrays")
	multiObjectValue := conf.GetObject("multi_objects")

	fmt.Printf("multiArrayValue: %v multiObjectValue: %v\n", multiArrayValue, multiObjectValue)

	extendVal := conf.GetArray("extendVal")
	fmt.Printf("extendVal: %v \n", extendVal)

}

func TestHconf2(t *testing.T) {
	conf, err := hocon.ParseResource("data/application_bc.conf")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	fmt.Println(conf.Get("app.version"))

	sinks := conf.GetArray("app.sinks")
	for _, sink := range sinks {
		fmt.Println(sink.(hocon.Object).ToConfig().GetString("topic"))
	}
}

func TestHconf3(t *testing.T) {
	conf, err := hocon.ParseResource("data/application_detect.conf")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	fmt.Println(conf.Get("app.version"))

	sinks := conf.GetArray("app.sinks")
	for _, sink := range sinks {
		fmt.Println(sink.(hocon.Object).ToConfig().GetString("topic"))
	}
}
