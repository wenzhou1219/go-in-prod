package compare_config

import (
	"fmt"
	"gopkg.in/ini.v1"
	"os"
	"testing"
)

// TestIni
// doc 参考 https://ini.unknwon.io/docs/intro/getting_started
// https://zhuanlan.zhihu.com/p/116753858
func TestIni(t *testing.T) {
	cfg, err := ini.Load("data/my.ini")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	// 典型读取操作(支持类型转换)，默认分区可以使用空字符串表示
	fmt.Println("App Mode:", cfg.Section("").Key("app_mode").String())
	fmt.Println("Server port:", cfg.Section("server").Key("http_port").MustInt(8080))

	// 我们可以做一些候选值限制的操作
	fmt.Println("Server Protocol:",
		cfg.Section("server").Key("protocol").In("http", []string{"http", "https"}))

	// 修改某个值然后进行保存
	cfg.Section("").Key("app_mode").SetValue("production")
	cfg.SaveTo("data/my.ini.local")
}

type Config struct {
	AppMode string `ini:"app_mode"`

	Server ServerConfig `ini:"server"`
}

type ServerConfig struct {
	Proto string `ini:"protocol"`
	Port  int    `ini:"http_port"`
}

func TestIniReflect(t *testing.T) {
	cfg, err := ini.Load("data/my.ini")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	// 初始化 默认值
	p := &Config{
		AppMode: "production",
		Server: ServerConfig{
			Proto: "http",
			Port:  8080,
		},
	}

	// 映射到结构体
	err = cfg.MapTo(p)
	if err != nil {
		fmt.Println("MapTo failed: ", err)
		return
	}

	fmt.Println(p)
}
